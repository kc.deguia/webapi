<?php

namespace Interfaces;

interface IEvent {

	public function beforeExecuteRoute($event,$app);
}
