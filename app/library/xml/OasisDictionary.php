<?php
namespace XML;


use Constants\oasisMos;

class oasisDictionary{

    // This Function will be used for validation later
    public static function regJson($xmlKey, $jsonFile){
        $dir = dirname(__FILE__);

        $jsonCollection = array();

        if(is_dir($dir)){
            if($dh = opendir($dir)){
                while(($file = readdir($dh)) != false){

                    if($file == "." or $file == ".."){

                    } else {

                        $info = pathinfo($dir."/".$file);

                        $fileBasename = $info['basename'];
                        $fileExtension = $info['extension'];
                        $fileFilename= $info['filename'];
                        if($fileExtension== "json"){
                            $jsonString = file_get_contents($dir."/".$fileBasename);
                            $jsonData = json_decode($jsonString, true);
                            $jsonCollection[$fileFilename] = $jsonData ;
                        }


                    }
                }
            }
            if(array_key_exists($jsonFile, $jsonCollection)){
                foreach ($jsonCollection[$jsonFile] as $itmMstrKey => $itmMstrVal) {
                    if($itmMstrVal['itm_id'] == $xmlKey){
                        var_dump($itmMstrVal);
                        break;
                    }

                }
            }
            // return $return_array;
        }
    }

    public static function optChk($consSec){

        $opt = oasisMos::formCode['06'];
        $chk = oasisMos::formCode['08'];

        $dataValue = [];

        foreach ($consSec as $mo => $constVal) {
            if($constVal['formCode']==$opt){
                $dataValue[$mo] = $constVal['option'];
            }else if ($constVal['formCode']==$chk) {
                $dataValue[$mo] = $constVal['chkgrp'];
            }
        }

        return $dataValue;

    }
}
