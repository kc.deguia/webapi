<?php

/**
 * Event that check JWT Authentication
 *
 * @package Events
 * @subpackage Api
 * @author Jete O'Keeffe
 * @version 1.0
 */

namespace Security;
use \Phalcon\Acl\Adapter\Memory as AclList;
class AclRoles {

    /**
     * Setup an Event
     *
     * Phalcon event to make sure client sends a valid message
     * @return FALSE|void
     */
    public static function setAcl($resources,$roles, $filename) {
        //Create the ACL
        $acl = new \Phalcon\Acl\Adapter\Memory();
        //The default action is DENY access
        $acl->setDefaultAction(\Phalcon\Acl::DENY);

        foreach ($roles as $role) {
            $acl->addRole(new \Phalcon\Acl\Role($role));
        }

        /*
         * RESOURCES
         * for each user, specify the 'controller' and 'method' they have access to (user=>[controller=>[method,method]],...)
         * this is created in an array as we later loop over this structure to assign users to resources
         * */

        foreach($resources as $arrResource){
            foreach($arrResource as $controller=>$arrMethods){
                $acl->addResource(new \Phalcon\Acl\Resource($controller),$arrMethods);
            }
        }

        /*
         * ACCESS
         * */
        foreach ($acl->getRoles() as $objRole) {
            $roleName = $objRole->getName();
                foreach ($resources[$roleName] as $resource => $method) {
                    $acl->allow($roleName,$resource,$method);
                }
        }

        try{
            $path = __DIR__ . "/../../cache/acl/";
            if(!is_dir($path)){
                throw new \Exception('Missing Cache');
            }

            return file_put_contents($path . $filename . ".data", serialize($acl));
        }
        catch (\Exception $e){
            throw new \Micro\Exceptions\HTTPExceptions(
                $e->getMessage(),
                500,
                array(
                    'dev' => 'Internal Error.',
                    'internalCode' => 'ACL0001',
                    'more' => ''
                )
            );
        }
    }

    public static function getAcl($user,$activeHandler, $handler){
        $path = __DIR__ . "/../../cache/acl/";

        // Restore acl object from serialized file

        $aclfile = $path . $user . "-ACL_RECORD.data";

        if(!is_file($aclfile)){
            throw new \Micro\Exceptions\HTTPExceptions(
                "Cannot read ACL data for the logged in user ". $user ,
                500,
                array(
                    'dev' => 'Internal Error.',
                    'internalCode' => 'ACL0001',
                    'more' => ''
                )
            );
        }

        $acl = unserialize(file_get_contents($aclfile));

        $allowed = $acl->isAllowed($user, $activeHandler, $handler);

        return $allowed;
    }

    public static function sourceAcl($member, $collections, $model){
        $rolesResources = array();

        foreach($member as $m){
            foreach($collections as $col){
                foreach($col->getHandlers() as $colh) {
                    $controller = str_replace('Controllers\\','',$col->getHandler());
                    if($m->employeetype=="Owner"){
                        $rolesResources[$m->memberid][$controller][] = $colh[2];
                    }else{


                     if(isset($colh[3])){
                        $sql = ["memberid=?0 AND role=?1", "bind"=>[$m->memberid, $colh[3]]];
                         if(is_array($colh[3])){
                            $bind = [];
                            $count = 1;
                            foreach ($colh[3] as $val) {
                                $bind[] = ' role=?' . $count;
                                $count++;
                            }

                            $data = array_merge([$m->memberid], $colh[3]);
                            $sql = ["memberid=?0 AND (".implode(' OR ', $bind).")", "bind"=> $data ];
                         }
                            $rec  = $model::findFirst($sql);
                            if($rec){
                                $rolesResources[$rec->memberid][$controller][] = $colh[2];
                            }
                     }

                    }
                }
            }
        }

        $msg = array();

        foreach ($rolesResources as $key => $value) {
            $msg[$key] = self::setAcl([$key => $value],[$key],  $key."-ACL_RECORD") ? "Successfully created your roles!": "Unsuccessfull";
        }

        return $msg;
    }

    public static function singleUserAcl($memberid, $collections, $model){
        $owner = false;
        if(empty($memberid)){
            throw new \Micro\Exceptions\HTTPExceptions(
                "Cannot generate ACL roles",
                406,
                array(
                    'dev' => 'Error Roles.',
                    'internalCode' => 'ACL0001',
                    'more' => ''
                )
            );

        }

        $own = \Models\Members::findFirst(
                    [
                        "employeetype = 'Owner' AND memberid = ?0",
                        "columns" => "memberid",
                        "bind" => [$memberid]
                    ]);

        if($own){
            $owner = true;
        }else{
            $rec  = $model::find(["memberid=?0", "bind" => [$memberid]]);

            if(!$rec){
                throw new \Micro\Exceptions\HTTPExceptions(
                    "No roles found.",
                    406,
                    array(
                        'dev' => 'Error Roles.',
                        'internalCode' => 'ACL0001',
                        'more' => ''
                    )
                );
            }
        }
        $rolesResources = [];

        foreach($collections as $col){

            foreach($col->getHandlers() as $colh) {

                $controller = str_replace('Controllers\\','',$col->getHandler());
                    if($owner){
                        $rolesResources[$own->memberid][$controller][] = $colh[2];
                    }else{
                        foreach($rec as $r){
                            if(is_array($colh[3])){
                                $currentRole = in_array($r->role , $colh[3]);
                             }else{
                                 $currentRole = ($colh[3] == $r->role ? true : false);
                             }

                             if($currentRole){

                                $rolesResources[$r->memberid][$controller][] = $colh[2];

                             }


                        }
                    }

            }
        }

        $data = array(self::setAcl($rolesResources,[$memberid], $memberid."-ACL_RECORD") ? "Successfully created your roles!": "Something went wrong");
        return $data;
    }

}
