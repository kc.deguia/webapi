<?php

/**
 * Event that check JWT Authentication
 *
 * @package Events
 * @subpackage Api
 * @author Jete O'Keeffe
 * @version 1.0
 */

namespace Security;

use Services\UserAgent;
use Models\UserBlacklists;
use Models\Loginattempts;

class Wap extends \Phalcon\DI\Injectable{

    protected $token;

    public function addToUserBlacklist($data){

        $ubsearch = UserBlacklists::findFirst([
                "userid = ?0", 
                "bind" =>[$data['id']]
            ]);

        $ub = $ubsearch ? $ubsearch : new UserBlacklists();
        
        $time = time();
        $ub->userid = $data['id'];
        $ub->reason = $data['reason'];
        $ub->expiration = date('Y-m-d H:i:s',strtotime('+'.$this->config['attempts']['expiration'], $time));
        $ub->ip_address = $this->request->getClientAddress();
        $ub->operating_system = $this->userAgent->getOS();
        $ub->browser = $this->userAgent->getBrowser();

        if(!$ub->save()){
            $this->ErrorMessage->thrower(
                    406,
                    "Cannot add to blacklist.",
                    "User Blacklisting Error.",
                    "MEM002"
                );
        }
        return true;
    }

    // Return if found on blacklist also check if expired
    public function firewallCheck($val,$arg = 'userid = ?'){
        //Create a User Blacklists to get username or by id here
        //So you have optio to check search by ID or by username
        $ubsearch = UserBlacklists::search($arg,['userid', 'expiration', 'username'], [$val]);

        if(!empty($ubsearch)){
            $datetime = strtotime($ubsearch[0]['expiration']);
            $time = time();
            if( $time < $datetime){
                Loginattempts::deleteAttempts($ubsearch[0]['userid'], 'all');

                $this->ErrorMessage->thrower(
                    403,
                    "Your account is locked! You have reached the maximum 5 attempts.",
                    "You are recieving this error due to your accound it recorded as blocked, pls wait for the expiration time to lift your account.",
                    "MEM002",
                    [
                        "exp" => date("Y-m-d H:i:s" , $datetime),
                        "time" => date("Y-m-d H:i:s" , $time),
                        "diff" => $datetime - $time
                    ]
                );
                return true;
            }
        }

        //Check if requires relogin
        if($this->redisWrapper->redisSIsMembers('hc_require_relogin',$val)){
            $this->ErrorMessage->thrower(
                403,
                "Relogin Required.",
                "You are recieving this error due to your account roles has been modified by the administrator.",
                "WAP-403",
                [
                    "reloginpopup" => true
                ]
            );
            return true;
        }

        return false;
    }

    public function getToken(){
        return $this->token;
    }

    public function tokenChecker(){
        $token = $this->request->getHeader("Authorization");

        if(empty($token)){
            $this->ErrorMessage->thrower(
                401,
                "Access Token Missing",
                "Forgot Password Error!",
                "MEM002",
                $this->errors
            );
        }

        try{
            $parsetoken = explode(" ",$token);

            $payload = \Firebase\JWT\JWT::decode($parsetoken[1], $this->config['hashkey'], array('HS256'));
            return $payload;
        }catch(\Exception $e){
            $this->ErrorMessage->thrower(
                401,
                $e->getMessage(),
                "Forgot Password Error!",
                "MEM002"
            );
        }
    }

    public function dateDifference($date_1 , $date_2 , $differenceFormat = '%i' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
        
        $interval = date_diff($datetime1, $datetime2);
        
        return $interval->format($differenceFormat);
        
    }

}