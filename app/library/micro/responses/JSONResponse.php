<?php
namespace Micro\Responses;


class JSONResponse extends \Micro\Responses\Response{
    protected $snake = true;
    protected $envelope = true;
    public function __construct(){
        parent::__construct();
    }
    public function send($records, $error=false){
        // Error's come from HTTPException.  This helps set the proper envelope data
        $response = $this->di->get('response');

        $success = ($error) ? 'ERROR' : 'SUCCESS';
        // If the query string 'envelope' is set to false, do not use the envelope.
        // Instead, return headers.
        $request = $this->di->get('request');
        if($request->get('envelope', null, null) === 'false'){
            $this->envelope = false;
        }
        // Most devs prefer camelCase to snake_Case in JSON, but this can be overriden here
        if($this->snake){
            $records = $this->convertKeysToCamelCase($records);
        }
        $etag = md5(serialize($records));
        if($this->envelope){
            // Provide an envelope for JSON responses.  '_meta' and 'records' are the objects.
            $message = array();
            $message['_meta'] = array(
                'status' => $success,
                'count' => ($error) ? 1 : count($records)
            );
            // Handle 0 record responses, or assign the records
            if($message['_meta']['count'] === 0){
                // This is required to make the response JSON return an empty JS object.  Without
                // this, the JSON return an empty array:  [] instead of {}
                $message['records'] = new \stdClass();
            } else {
                $message['records'] = $records;
            }
        } else {
            $response->setHeader('X-Record-Count', count($records));
            $response->setHeader('X-Status', $success);
            $message = $records;
        }

        //Check if ResponseCode exist then replace status code

        if (array_key_exists("ResponseCode",$records)){
            $response->setStatusCode($records['ResponseCode'], \Micro\Exceptions\HTTPExceptions::$mess);
        }elseif(array_key_exists("ApiCode",$records)){
            $response->setStatusCode($records['ApiCode'], \Micro\Exceptions\HTTPExceptions::$mess);
        }

        $response->setContentType('application/json');
        $response->setEtag($etag);
        $response->setJsonContent($message);
        $response->send();
        return $this;
    }
    public function convertSnakeCase($snake){
        $this->snake = (bool) $snake;
        return $this;
    }
    public function useEnvelope($envelope){
        $this->envelope = (bool) $envelope;
        return $this;
    }
}