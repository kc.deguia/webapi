<?php

/**
 * Event that check JWT Authentication
 *
 * @package Events
 * @subpackage Api
 * @author Jete O'Keeffe
 * @version 1.0
 */

namespace Events\Api;

use Interfaces\IEvent as IEvent;
class TokenAuthentication extends \Phalcon\Events\Manager implements IEvent {

    /**
     * Hmac Message
     * @var object
     */
    protected $_token;

    /**
     * Private key for HMAC
     * @var string
     */
    protected $_privateKey;

    /**
     * Constructor
     *
     * @param object
     * @param string
     */
    public function __construct($message, $privateKey) {
        $this->_token = $message;
        $this->_privateKey = $privateKey;
    }

    /**
     * Setup an Event
     *
     * Phalcon event to make sure client sends a valid message
     * @return FALSE|void
     */
    public function beforeExecuteRoute($event,$app) {
        // Check if it does not need authentication return and skip JWT Checking
        $method = strtolower($app->router->getMatchedRoute()->getHttpMethods());

        $unAuthenticated = $app->getUnauthenticated();
        if (isset($unAuthenticated[$method])) {
            $unAuthenticated = array_flip($unAuthenticated[$method]);
            if (isset($unAuthenticated[$app->router->getMatchedRoute()->getPattern()])) {
                return true;
            }
        }
        // End of checking unAuthenticated

        // Start of JWT Authentication
        // Authenticating the Token If expired or something is wrong in decoding

        $parsetoken = explode(" ",$this->_token);
        $token = \Firebase\JWT\JWT::decode($parsetoken[1], $this->_privateKey, array('HS256'));

        if($token){
            $app->di->getShared('User')->setUser($token);
            return $token;
        }

        // End of JWT Authentication
        throw new \Micro\Exceptions\HTTPExceptions(
            'Unauthorized Access',
            401,
            array(
                'dev' => 'Accessing this route is not permitted.',
                'internalCode' => 'NF5000',
                'more' => 'Pass a correct token.'
            )
        );
//                return false;

    }
}
