<?php

namespace Services;

use Phalcon\Paginator\AdapterInterface as PaginatorInterface;

class PaginatorQuery implements PaginatorInterface {

	private $offset;
	private $phql 		= "";
	private $params 	= [];
	private $page;
	public $limit;
	
	public function __construct(array $config) {
		$this->phql 	= $config['query'];
		$this->params 	= $config['params'];

		$this->setCurrentPage($config['page']);
		$this->setLimit($config['limit']);
        $this->offset = ($this->page * $this->limit) - $this->limit;

        /*  */
        if(!strpos($this->phql, "SQL_CALC_FOUND_ROWS")) {
            $this->phql = preg_replace(
            	'/SELECT/',
            	'SELECT SQL_CALC_FOUND_ROWS',
            	$this->phql, 1
            );
        }

        /*  */
        $this->phql  .= " LIMIT $this->offset, $this->limit";
	} 

	public function setCurrentPage($page) {
		$this->page = $page;
	}

	public function setLimit($limit) {
		$this->limit = $limit;
	}

	public function getLimit() {
		return $this->limit;
	}

	private function bindParams($stmt) {
		foreach ($this->params as $param) {
			$stmt->bindParam(
            	$param['keyword'],
            	$param['value'],
            	($param['type'] == 'int') ? \PDO::PARAM_INT : \PDO::PARAM_STR
            );
        }

        return $stmt;
	}

	public function withoutTrashedFrom($tables) {
		$condition = "WHERE (";
		foreach ($tables as $idx => $value) {
			$condition .= ($idx > 0) ? " AND $value.dlt = 0" : "$value.dlt = 0";
		}
		$condition .= ") AND ";

		$this->phql = preg_replace('~WHERE(?!.*WHERE)~', $condition, $this->phql);
		return $this;
	}

	public function getPaginate() {
        $db 	= \Phalcon\DI::getDefault()->get('db');
        $stmt 	= $db->prepare($this->phql);
        $stmt   = (count($this->params) > 0) ? $this->bindParams($stmt) : $stmt;
        $stmt->execute();

        $this->items       = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $this->totalItems  = $db->query("SELECT FOUND_ROWS()")->fetch(\PDO::FETCH_COLUMN);
        $this->totalPages  = ceil($this->totalItems / $this->limit);
        $this->current     = $this->page;

		return $this;
	}

}