<?php

namespace Services;

Class Mailer {


	protected $config;

	public function __construct($config){
		$this->config = $config;
	}

    // send email using postmark
    public function sendMail($email, $subject, $content, $template) {


		if($template){
			$body = $this->$template($content);
		}else{
			$body = "Empty Body";
		}

        $json = json_encode(array(
            'From' => $this->config['postmark']['signature'],
            'To' => $email,
            'Name' => 'Medisource',
            'Subject' => $subject,
            'HtmlBody' => $body
        ));

        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $this->config['postmark']['url']);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$this->config['postmark']['token']
        ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = curl_exec($ch2);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);

        return $http_code;
    }


    private function passwordTemplate($emailcontent){

        $content =
            '<html lang=en>
		<head>
			<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
			<meta name=viewport content="width=device-width, initial-scale=1">
			<meta http-equiv=X-UA-Compatible content=IE=edge>
			<meta name=format-detection content=telephone=no>
			<title>Body and Brain</title>

			<style type=text/css>
				body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
				table{border-spacing: 0;}table td{border-collapse: collapse;}
				.ExternalClass{width: 100%;}
				.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
				.ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
				.yshortcuts a {border-bottom: none !important;}
				@media screen and (max-width: 599px) {
					.force-row,.container {width: 100% !important;max-width: 100% !important;}
				}
				@media screen and (max-width: 400px) {
					.container-padding {padding-left: 12px !important;padding-right: 12px !important;}
				}
				.ios-footer a {color: #aaaaaa !important;
					text-decoration: underline;}
				</style>
			</head>
			<body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
				<table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0>
					<tr>
						<td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;">
							<br>
							<table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px>
								<tr>
									<td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#49AFCD;padding-left:24px;padding-right:24px">Medisource</td>
								</tr>
								<tr>
									<td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff>
										<br>
										<div class=title style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#49AFCD">Password Reset Information</div>
										<br>
										<div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:20px;text-align:left;color:#333333">This email was sent because the administrator reset your password. Below is your new credential password:
											<br><br>
										</div>

										<div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:20px;text-align:left;color:#333333">
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:600;color:#49AFCD">Username: </span>
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:600;">'.$emailcontent["username"].'</span>
											<br>
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:600;color:#49AFCD">Password: </span>
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:600;">'.$emailcontent["password"].'<i>(temporary password)</i></span>
											<br><br>
										</div>

										<div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:20px;text-align:left;color:#333333;margin-bottom:10px;">
											<span>To login your account please click the button bellow or copy and paste the link to browser.</span>
											<br>
											<br>
											<span>After you login please change your temporary password.</span>
											<br>
											<br>
											<a href="'.$this->config['application']['baseURL'].'/login'.'" style="background:#49AFCD;color:#FFF;padding:10px;text-decoration: none;border-radius:5px;font-size:14px;">Login Page</a>
										</div>

									</td>
								</tr>
								<tr>
									<td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
										<br><br>
										Copyright: © Medisource
										<br><br>
										<strong>Medisource</strong>
										<br>
										<a href="http://www.bodynbrain.com" style=color:#aaaaaa>www.medisource.com</a>
										<br> <br><br>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
			</html>';

        return $content;
    }

	private function loginTemplate($emailcontent){

        $content =
            '<html lang=en>
		<head>
			<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
			<meta name=viewport content="width=device-width, initial-scale=1">
			<meta http-equiv=X-UA-Compatible content=IE=edge>
			<meta name=format-detection content=telephone=no>
			<title>Body and Brain</title>

			<style type=text/css>
				body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
				table{border-spacing: 0;}table td{border-collapse: collapse;}
				.ExternalClass{width: 100%;}
				.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
				.ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
				.yshortcuts a {border-bottom: none !important;}
				@media screen and (max-width: 599px) {
					.force-row,.container {width: 100% !important;max-width: 100% !important;}
				}
				@media screen and (max-width: 400px) {
					.container-padding {padding-left: 12px !important;padding-right: 12px !important;}
				}
				.ios-footer a {color: #aaaaaa !important;
					text-decoration: underline;}
				</style>
			</head>
			<body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
				<table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0>
					<tr>
						<td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;">
							<br>
							<table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px>
								<tr>
									<td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#49AFCD;padding-left:24px;padding-right:24px">Medisource</td>
								</tr>
								<tr>
									<td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff>
										<br>
										<div class=title style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#49AFCD">Login Information</div>
										<br>
										<div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:20px;text-align:left;color:#333333">This email was sent in response to your account credential for you to be able to login on our software. In order to login.
											<br><br>
										</div>

										<div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:20px;text-align:left;color:#333333">
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:600;color:#49AFCD">Username: </span>
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:600;">'.$emailcontent["username"].'</span>
											<br>
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:600;color:#49AFCD">Password: </span>
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:600;">'.$emailcontent["password"].'<i>(temporary password)</i></span>
											<br><br>
										</div>

										<div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:20px;text-align:left;color:#333333;margin-bottom:10px;">
											<span>To login your account please click the button bellow or copy and paste the link to browser.</span>
											<br>
											<br>
											<span>After you login please change your temporary password.</span>
											<br>
											<br>
											<a href="'.$this->config['application']['baseURL'].'/login'.'" style="background:#49AFCD;color:#FFF;padding:10px;text-decoration: none;border-radius:5px;font-size:14px;">Login Page</a>
										</div>

									</td>
								</tr>
								<tr>
									<td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
										<br><br>
										Copyright: © Medisource
										<br><br>
										<strong>Medisource</strong>
										<br>
										<a href="http://www.bodynbrain.com" style=color:#aaaaaa>www.medisource.com</a>
										<br> <br><br>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
			</html>';

        return $content;
    }

    private function addTemplate($emailcontent){

        $content =
			'<html>
			<head>

			    <title>Medisource</title>
			    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
			    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			        
			        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
			        

			    <style type="text/css">
			        .ReadMsgBody { width: 100%; background-color: #f7f7f7; }
			        .ExternalClass { width: 100%; background-color: #f7f7f7; }
			        body { width: 100%; background-color: #f7f7f7; margin: 0; padding: 0; -webkit-font-smoothing: antialiased; font-family: Arial, Times, serif }
			        table { border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			        @-ms-viewport{ width: device-width; }

			        @media only screen and (max-width: 639px){
			        .wrapper{ width:100%;  padding: 0 !important; }
			        }

			        @media only screen and (max-width: 480px){
			        .centerClass{ margin:0 auto !important; }
			        .imgClass{ width:100% !important; height:auto; }
			        .wrapper{ width:320px; padding: 0 !important; }
			        .header{ width:320px; padding: 0 !important; background-image: url(http://placehold.it/320x400) !important; }
			        .container{ width:300px;  padding: 0 !important; }
			        .mobile{ width:300px; display:block; padding: 0 !important; text-align:center !important;}
			        .mobile50{ width:300px; padding: 0 !important; text-align:center; }
			        *[class="mobileOff"] { width: 0px !important; display: none !important; }
			        *[class*="mobileOn"] { display: block !important; max-height:none !important; }
			        }

			    </style>

			</head>
			<body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" style="background-color:#f7f7f7;  font-family:Arial,serif; margin:0; padding:0; min-width: 100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
			    <img style="min-width:640px; display:block; margin:0; padding:0" class="mobileOff" width="640" height="1" src="'.$this->config['application']['baseURL'].'/assets/img/spacer.gif">
			    <table>
			        <tr>
			            <td height="20" style="font-size:10px; line-height:10px;"> </td>
			        </tr>
			    </table>
			    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#f7f7f7">
			        <tr>
			            <td width="100%" valign="top" align="center">
			                <table width="800" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#2196f3">
			                    <tr>
			                        <td height="20" style="font-size:10px; line-height:10px;"> </td>
			                    </tr>
			                    <tr>
			                        <td align="center">

			                            
			                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
			                                <tr>
			                                    <td class="mobile" style="font-family:arial; font-size:12px; line-height:18px;" align="center">
			                                        <img src="'.$this->config['application']['baseURL'].'/assets/img/medisource-white.png" width="150" height="" style="margin:0; padding:0; border:none; display:block;" border="0" class="centerClass" alt="" />
			                                    </td>
			                                </tr>
			                            </table>
			                            

			                        </td>
			                    </tr>
			                    <tr>
			                        <td height="20" style="font-size:10px; line-height:10px;"> </td>
			                    </tr>
			                </table>
			                <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper" bgcolor="#ffffff" style="box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15">
			                    <tbody>
			                        <tr>
			                            <td height="40" style="line-height:20px; font-size:20px;"> </td>
			                        </tr>
			                        <tr>
			                            <td align="center" bgcolor="#ffffff">

			                                
			                                <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
			                                    <tr>
			                                        <td align="center" class="mobile" style="font-family: Arial; font-size:24px; color:#2196f3;line-height:26px;">
			                                                <strong>Your Medisource login details</strong>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                    <tr>
			                                        <td align="center" style="font-family: Arial; font-size: 16px; color: #4d4d4d; line-height:18px; padding:15px;">
			                                            Now You are in! This email was sent in response to your account credentials for you to be able to login on our software.
			                                            <br><br>
			                                            We have created a password for you, but we strongly urge you to change it for your own security.
			                                        </td>
			                                    </tr>
			                                </table>
			                                <table width="300" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                    <tr>
			                                        <td align="left" bgcolor="#eeeeee" style="font-family: Arial; font-size: 14px; color: #4d4d4d; line-height:18px; padding:15px;">
			                                            <span style="color:#9e9e9e;">Username:</span>&nbsp;&nbsp;
			                                            <span style="color:#454545; font-size:18px;">'.$emailcontent["username"].'</span>
			                                            <br><br>
			                                            <span style="color:#9e9e9e;">Temporary Password:</span>&nbsp;&nbsp;
			                                            <span style="color:#454545; font-size:18px;">'.$emailcontent["password"].'</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                </table>
			                                
			                                <table width="170" cellpadding="0" cellspacing="0" align="center" border="0">
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                    <tr>
			                                        <td width="170" height="36" bgcolor="#ff5252" align="center" valign="middle" style="border-radius:5px;font-family: Arial; font-size: 14px; color: #ffffff; line-height:20px;">
			                                            <a href="'.$this->config['application']['baseURL'].'/login'.'" target="_blank" alias="" style="font-family: Arial; text-decoration: none; color: #ffffff; text-transform:uppercase; font-weight:bold; letter-spacing:1px;">Login</a>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                </table>
			                                
			                                <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="mobile" style="font-family:arial; font-size:12px; line-height:18px;" align="center">
			                                            <img src="'.$this->config['application']['baseURL'].'/assets/img/medisource-black.png" width="100" height="" style="margin:0; padding:0; border:none; display:block;" border="0" class="centerClass" alt="" />
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="mobile" style="font-family:arial; font-size:12px; line-height:18px; color:#757575;" align="center">
			                                            Medisource. All rights reserve &copy; 2016
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="mobile" style="font-family:arial; font-size:12px; line-height:18px;" align="center">
			                                            <a href="" target="_blank"style="font-size: 12px;line-height: 18px;color: #757575;text-decoration: underline;">www.medisource.com</a>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>
			                    </tbody>
			                </table>
			            </td>
			        </tr>
			    </table>
			    <table>
			        <tr>
			            <td height="20" style="font-size:10px; line-height:10px;"> </td>
			        </tr>
			    </table>

			</body>
			</html>';
        return $content;
    }

private function securityEmailTemplate($emailcontent){

        $content =
			'<html>
			<head>

			    <title>Medisource</title>
			    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
			    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			        
			        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
			        

			    <style type="text/css">
			        .ReadMsgBody { width: 100%; background-color: #f7f7f7; }
			        .ExternalClass { width: 100%; background-color: #f7f7f7; }
			        body { width: 100%; background-color: #f7f7f7; margin: 0; padding: 0; -webkit-font-smoothing: antialiased; font-family: Arial, Times, serif }
			        table { border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			        @-ms-viewport{ width: device-width; }

			        @media only screen and (max-width: 639px){
			        .wrapper{ width:100%;  padding: 0 !important; }
			        }

			        @media only screen and (max-width: 480px){
			        .centerClass{ margin:0 auto !important; }
			        .imgClass{ width:100% !important; height:auto; }
			        .wrapper{ width:320px; padding: 0 !important; }
			        .header{ width:320px; padding: 0 !important; background-image: url(http://placehold.it/320x400) !important; }
			        .container{ width:300px;  padding: 0 !important; }
			        .mobile{ width:300px; display:block; padding: 0 !important; text-align:center !important;}
			        .mobile50{ width:300px; padding: 0 !important; text-align:center; }
			        *[class="mobileOff"] { width: 0px !important; display: none !important; }
			        *[class*="mobileOn"] { display: block !important; max-height:none !important; }
			        }

			    </style>

			</head>
			<body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" style="background-color:#f7f7f7;  font-family:Arial,serif; margin:0; padding:0; min-width: 100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
			    <img style="min-width:640px; display:block; margin:0; padding:0" class="mobileOff" width="640" height="1" src="'.$this->config['application']['baseURL'].'/assets/img/spacer.gif">
			    <table>
			        <tr>
			            <td height="20" style="font-size:10px; line-height:10px;"> </td>
			        </tr>
			    </table>
			    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#f7f7f7">
			        <tr>
			            <td width="100%" valign="top" align="center">
			                <table width="800" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#2196f3">
			                    <tr>
			                        <td height="20" style="font-size:10px; line-height:10px;"> </td>
			                    </tr>
			                    <tr>
			                        <td align="center">

			                            
			                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
			                                <tr>
			                                    <td class="mobile" style="font-family:arial; font-size:12px; line-height:18px;" align="center">
			                                        <img src="'.$this->config['application']['baseURL'].'/assets/img/medisource-white.png" width="150" height="" style="margin:0; padding:0; border:none; display:block;" border="0" class="centerClass" alt="" />
			                                    </td>
			                                </tr>
			                            </table>
			                            

			                        </td>
			                    </tr>
			                    <tr>
			                        <td height="20" style="font-size:10px; line-height:10px;"> </td>
			                    </tr>
			                </table>
			                <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper" bgcolor="#ffffff" style="box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15">
			                    <tbody>
			                        <tr>
			                            <td height="40" style="line-height:20px; font-size:20px;"> </td>
			                        </tr>
			                        <tr>
			                            <td align="center" bgcolor="#ffffff">

			                                
			                                <table width="600" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
			                                    <tr>
			                                        <td align="center" class="mobile" style="font-family: Arial; font-size:24px; color:#2196f3;line-height:26px;">
			                                                <strong> Medisource Account Security Update</strong>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                    <tr>
			                                        <td align="center" style="font-family: Arial; font-size: 16px; color: #4d4d4d; line-height:18px; padding:15px;">
			                                            You have successfuly updated the Security of your account <strong>'.$emailcontent["username"].'</strong>!
			                                            <br><br>
			                                            
			                                        </td>
			                                    </tr>
			                                </table>
			                                
			                                <table width="170" cellpadding="0" cellspacing="0" align="center" border="0">
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                    <tr>
			                                        <td width="170" height="36" bgcolor="#ff5252" align="center" valign="middle" style="border-radius:5px;font-family: Arial; font-size: 14px; color: #ffffff; line-height:20px;">
			                                            <a href="'.$this->config['application']['baseURL'].'/login'.'" target="_blank" alias="" style="font-family: Arial; text-decoration: none; color: #ffffff; text-transform:uppercase; font-weight:bold; letter-spacing:1px;">Login</a>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                </table>
			                                
			                                <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="mobile" style="font-family:arial; font-size:12px; line-height:18px;" align="center">
			                                            <img src="'.$this->config['application']['baseURL'].'/assets/img/medisource-black.png" width="100" height="" style="margin:0; padding:0; border:none; display:block;" border="0" class="centerClass" alt="" />
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="mobile" style="font-family:arial; font-size:12px; line-height:18px; color:#757575;" align="center">
			                                            Medisource. All rights reserve &copy; 2016
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="mobile" style="font-family:arial; font-size:12px; line-height:18px;" align="center">
			                                            <a href="" target="_blank"style="font-size: 12px;line-height: 18px;color: #757575;text-decoration: underline;">www.medisource.com</a>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td height="20" style="line-height:20px; font-size:20px;"> </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>
			                    </tbody>
			                </table>
			            </td>
			        </tr>
			    </table>
			    <table>
			        <tr>
			            <td height="20" style="font-size:10px; line-height:10px;"> </td>
			        </tr>
			    </table>

			</body>
			</html>';
        return $content;
    }

    function fpPINTemplate($pin){
    	$content = $this->headerTemplate();

    	$content .= '<tr>
									<td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff>
										<br>
										<div class=title style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#49AFCD">
											Forgot Password Reset Request PIN
										</div>
										<br>

										<div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:20px;text-align:left;color:#333333">
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:600;color:#49AFCD">PIN: </span>
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:600;">'.$pin.'</span>
											<br>
										</div>

									</td>
								</tr>';

    	$content .= $this->footerTemplate();
    	return $content;
    }

    function fpPasswordChange($username){
    	$content = $this->headerTemplate();

    	$content .= '<tr>
									<td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff>
										<br>
										<div class=title style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#49AFCD">
											Forgot Password Reset Request
										</div>
										<br>

										<div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:20px;text-align:left;color:#333333">
											<span style="font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:600;color:#49AFCD">Your account password for <strong>'.$username.'</strong> has been successfuly changed. </span>
											<br>
										</div>

									</td>
								</tr>';

    	$content .= $this->footerTemplate();
    	return $content;    	
    }

    protected function headerTemplate(){
    	return '<html lang=en>
		<head>
			<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
			<meta name=viewport content="width=device-width, initial-scale=1">
			<meta http-equiv=X-UA-Compatible content=IE=edge>
			<meta name=format-detection content=telephone=no>
			<title>Body and Brain</title>

			<style type=text/css>
				body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
				table{border-spacing: 0;}table td{border-collapse: collapse;}
				.ExternalClass{width: 100%;}
				.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
				.ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
				.yshortcuts a {border-bottom: none !important;}
				@media screen and (max-width: 599px) {
					.force-row,.container {width: 100% !important;max-width: 100% !important;}
				}
				@media screen and (max-width: 400px) {
					.container-padding {padding-left: 12px !important;padding-right: 12px !important;}
				}
				.ios-footer a {color: #aaaaaa !important;
					text-decoration: underline;}
				</style>
			</head>
			<body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
				<table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0>
					<tr>
						<td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;">
							<br>
							<table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px>
								<tr>
									<td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#49AFCD;padding-left:24px;padding-right:24px">Medisource</td>
								</tr>';
    }

	protected function footerTemplate(){
    	return '<tr>
									<td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
										<br><br>
										Copyright: © Medisource
										<br><br>
										<strong>Medisource</strong>
										<br>
										<a href="http://www.bodynbrain.com" style=color:#aaaaaa>www.medisource.com</a>
										<br> <br><br>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
			</html>';
    }

}