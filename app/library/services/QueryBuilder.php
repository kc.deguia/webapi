<?php

namespace Services;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class QueryBuilder
{
    protected $_query = "";

    protected $_columns = array();
    protected $_from = "";
    protected $_model = "";
    protected $_leftJoin = array();
    protected $_innerJoin = array();
    protected $_conditions = array();
    protected $_order = array();
    protected $_limit = array();
    protected $_params = array();
    protected $_errors = array();
    protected $_group = array();

    public function __construct() {
        //
    }

    public function columns($columns = array()) {
        $this->_columns = array_merge($this->_columns, $columns);
        
        return $this;
    }

    public function from($from = "", $model = "") {
        if(empty($from) && empty($model)) {
            $this->_errors[] = "You should only select 1 table";
            return $this;
        }

        $this->_from = $from;
        $this->_model = $model;

        return $this;
    }

    public function where($condition = "", $params = array(), $option = "") {
        if(!empty($condition) || !empty($params)) {
            $this->_conditions[] = $option . "(" . $condition . ")";
            if(!empty($params)) $this->_params = array_merge($this->_params, $params);
        }

        return $this;
    }

    public function andWhere($condition = "", $params = array()) {
        $this->where($condition, $params, $option = "AND ");

        return $this;
    }

    public function orWhere($condition = "", $params = array()) {
        $this->where($condition, $params, $option = "OR ");

        return $this;
    }

    public function leftJoin($table = "", $on = "", $params = array()) {
        if(!empty($table) && !empty($on)) {
            $this->_leftJoin[] = array(
                'table' => $table,
                'on' => $on
            );
        }
        if(!empty($params)) $this->_params = array_merge($this->_params, $params);

        return $this;
    }

    public function innerJoin($table = "", $on = "", $params = array()) {
        if(!empty($table) && !empty($on)) {
            $this->_innerJoin[] = array(
                'table' => $table,
                'on' => $on
            );
        }
        if(!empty($params)) $this->_params = array_merge($this->_params, $params);
        
        return $this;
    }

    public function groupBy($group = array()) {
        $this->_group = array_merge($this->_group, $group);

        return $this;
    }

    public function orderBy($order = array(), $params = array()) {
        $this->_order = array_merge($this->_order, $order);
        if(!empty($params)) $this->_params = array_merge($this->_params, $params);

        return $this;
    }

    public function limit($off = 0, $num = 0) {
        if(!empty($num) || !empty($off)) {
            $this->_limit = array(
                'num' => $num,
                'off' => $off
            );
        }

        return $this;
    }

    public function _checkParams() {
        return $this->_params;
    }

    protected function _buildQuery() {
        if(empty($this->_columns)) {
            $this->_columns[] = "*";
            $this->_query .= "SELECT * ";
        } else {
            $this->_query .= "SELECT " . implode(", ", $this->_columns) . " ";
        }

        if(empty($this->_from)) {
            $this->_errors[] = "No table selected";
            return $this;
        } else {
            $this->_query .= "FROM " . $this->_from . " ";
        }

        if(!empty($this->_innerJoin)) {
            foreach($this->_innerJoin as $innerJoin) {
                $this->_query .= "INNER JOIN " . $innerJoin['table'] . " ON " . $innerJoin['on'] . " ";
            }
        }

        if(!empty($this->_leftJoin)) {
            foreach($this->_leftJoin as $leftJoin) {
                $this->_query .= "LEFT JOIN " . $leftJoin['table'] . " ON " . $leftJoin['on'] . " ";
            }
        }

        if(!empty($this->_conditions)) {
            $this->_query .= "WHERE ";
            $this->_query .= implode(" ", $this->_conditions) . " ";
        }

        if(!empty($this->_group)) {
            $this->_query .= "GROUP BY " . implode(", ", $this->_group) . " ";
        }

        if(!empty($this->_order)) {
            $this->_query .= "ORDER BY " . implode(", ", $this->_order) . " ";
        }

        if(!empty($this->_limit)) {
            $this->_query .= "LIMIT " . $this->_limit['off'] . ", " . $this->_limit['num'] . " ";
        }
    }

    public function prepare()
    {
        $this->_buildQuery();

        return $this;
    }

    public function getQuery()
    {
        $this->_buildQuery();

        return $this->_query;
    }

    public function query($stmt, $model, $params)
    {
        if(empty($stmt)) { return []; }

        $class = "Models\\" . $model . "";
        $model = new $class();
        $result = $model->getReadConnection()->query($stmt, $params);
        return new Resultset(null, $model, $result);
    }

    public function execute()
    {
        if(!empty($this->_errors)) {
            return [];
        }

        $class = "Models\\" .$this->_model . "";
        $model = new $class();
        $result = $model->getReadConnection()->query($this->_query, $this->_params);
        return new Resultset(null, $model, $result);
    }
}