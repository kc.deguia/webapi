<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 07/07/2016
 * Time: 4:53 PM
 */

namespace Services;

class ErrorMessages
{
    protected $errorMessages;

    public function __construct(){
        $this->errorMessages = [
            "userExist" => "The user already exist.",
            "uniqueData" => "Data requested should only be unique."
        ];
    }

    public function thrower($code, $message,$dev = '', $internalcode='',$more=''){
        throw new \Micro\Exceptions\HTTPExceptions(
            $message,
            $code,
            array(
                'dev' => $dev,
                'internalCode' => $internalcode,
                'more' => $more
            )
        );
    }

    public function userExist($message,$other=null,$internalCode=null){
        $this->thrower(409,$message, $this->errorMessages["uniqueData"],$internalCode, $other);
    }

    public function conditionFailed($message="Condition Failed.", $other=null,$internalCode = null){
        $this->thrower(412,$message, 'Data requested should only be unique.',$internalCode, $other);
    }

    // Throw Form Validate
    public function throwValidate($validation, $userMessage, $devMessage, $errorCode){
        function errorObjectToArray($messages){
            $errormessage = [];
            foreach ($messages as $message) {
                $errormessage[] = [
                    $message->getField() => $message->getMessage()
                ];
            }
            return $errormessage;
        }
        $messages = $validation->validate($_POST);
        if(count($messages)) {
            $this->thrower(
            406,
            $userMessage,
            $devMessage,
            $errorCode,
            errorObjectToArray($messages));
        }
    }

    // Delete Failed
    public function deleteFailed($internalCode = null, $other=null){
        $this->thrower(422, "Cannot Delete Record.", 'Data requested should only be unique.',$internalCode, $other);
    }

}
