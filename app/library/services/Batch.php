<?php
 namespace Services;

class Batch {

    /*
    * OASIS Checkbox ~ slctLoop function
    * @param1 = Parent Table ID
    * @param2 = Foreign key name
    * @param3 = Array (Selected Checkboxes)
    * @param4 = Table Name
    * @param5 = Column Name
    */
    public function Insert($id, $colname, $dataValue, $table, $col){

        try {
            $db = \Phalcon\DI::getDefault()->get('db');
            $db->begin();
            $db->getTransactionLevel();

            $queryDlt = 'DELETE  FROM '.$table.' WHERE '.$colname.'="'.$id.'"';
            $delete = $db->prepare($queryDlt);
            $resultDlt = $delete->execute();

            if($resultDlt){
                $loopCount = 1;
                $arrayQuery='';
                if($dataValue && count($dataValue)!=0){
                    foreach ($dataValue as $key => $value){
                        $comma = $loopCount <  count($dataValue)? ',':'';
                        $arrayQuery.='("'.$id.'", "'.$value.'")'.$comma;
                        $loopCount++;
                    }

                    // INSERT SELECTED DATA
                    $queryInsert ='INSERT INTO '.$table.' ('.$colname.', '.$col.') VALUES '.$arrayQuery;
                    $insert = $db->prepare($queryInsert);
                    $resultSave = $insert->execute();

                    if(!$resultSave){
                        $db->rollback();
                    }
                }
            }else{
                $db->rollback();
            }

            return $db;
        }
        catch (TxFailed $e) {
            var_dump($e);
        }

    }
}
