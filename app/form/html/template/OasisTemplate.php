<?php
namespace Form\Template;
/**
 *NOTE: KINDLY DO NOT USE AUTO INDENT:
 */
class OasisTemplate{
    /**
     * [inputToLastMo description]
     * @param  [type] $details [Template Details]
     * @return [type]          [Template View]
     */
    public static function inputToLastMo($details){
        return '
        <div class="rTable">
            <div class="rTableBody">
                <div class="rTableRow">
                    <div class="rtCellInput tcell-230 tcell230-1198px">
                        <div class="title-mo font-b">
                            <span class="pull-left">'.$details['mo'].'</span>
                            <div class="mo-l-60">
                                '.$details['titleB'].'
                            </div>
                        </div>
                    </div>
                    <div class="rtCellInput p-r-10">
                        <text-input txt-name="formHcare" txt-info="'.$details['model'].'" txt-holder="CMS Certification #" ng-model="pi.'.$details['model'].'" ></text-input>
                    </div>
                </div>
            </div>
        </div>
        ';
    }

    public static function moTxtLabel($details){
        return '
        <div id="M0018" class="font-b">
            <span class="mo-span">'.$details['mo'].'</span>
            <div class="m-l-60">
                '.$details['titleB'].'
                <lable class="f-w-n">
                    '.$details['titleN'].'
                </lable>
            </div>
        </div>
        ';
    }


    public static function inputLast(){

    }




}
