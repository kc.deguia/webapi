<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;

use Models\Patient;

class ValPatientAdmistion extends Validation
{
    public function initialize()
    {
        // Checking that must be required
        $this->add("socDate",              new PresenceOf(["message" => " Start of Care Date is required",]));
        $this->add("episodeStartDate",     new PresenceOf(["message" => "Episode Start Date is required",]));

        //Expiration Date
        $this->add("socDate", new Date(["format" => "Y-m-d","message" => "Start of Care Date is not a valid date or format"]));
        $this->add("episodeStartDate", new Date(["format" => "Y-m-d","message" => "Episode Start Date is not a valid date or format"]));
    }
}
