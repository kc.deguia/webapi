<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;

class Storeorder extends Validation
{
    public function initialize()
    {
        //Checking that must be required
    	
    	$this->add("orderdate", new PresenceOf(["message" => "Orderdate is required",]));
    	$this->add("physician", new PresenceOf(["message" => "Physician ordered is required",]));
    	$this->add("communication", new PresenceOf(["message" => "Communication is required",]));
    	$this->add("finalfrequencyarray", new PresenceOf(["message" => "Frequency is required",]));
    }

}