<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;

class ValPatientCareDocStatus extends Validation
{
    public function initialize()
    {
        // Checking that must be required
        $this->add("type",              new PresenceOf(["message" => "Type is required",]));
        $this->add("foreignid",     new PresenceOf(["message" => "Foreign Id is required",]));
        $this->add("idpatient",     new PresenceOf(["message" => "Patient Id is required",]));
    }
}
