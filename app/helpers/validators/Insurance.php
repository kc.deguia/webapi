<?php
namespace RequestValidator;


use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;


class Insurance extends Validation
{
    public function initialize()
    {
        // Checking that must be required
        // $this->add("daac", new PresenceOf(["message" => "Display Allowed Amount on Claim is required",]));
        // $this->add("clearingHouse", new PresenceOf(["message" => "Clearing House is required",]));
        // $this->add("irid", new PresenceOf(["message" => "Intechange Reciever ID is required",]));
        // $this->add("payerId", new PresenceOf(["message" => "Payer ID is required",]));
        // $this->add("payerType", new PresenceOf(["message" => "Payer Type is required",]));
        // $this->add("invoiceType", new PresenceOf(["message" => "Invoice Type is required",]));
        // $this->add("parentInsurance", new PresenceOf(["message" => "Parent Insurance is required",]));
        $this->add("insuranceName", new PresenceOf(["message" => "Insurance Name is required",]));
        // $this->add("address", new PresenceOf(["message" => "Address is required",]));
        // $this->add("zipCode", new PresenceOf(["message" => "Zip Code is required",]));
        // $this->add("city", new PresenceOf(["message" => "City is required",]));
        // $this->add("state", new PresenceOf(["message" => "State is required",]));
        // $this->add("providerCode", new PresenceOf(["message" => "Provider Code is required",]));
        // $this->add("providerNumber", new PresenceOf(["message" => "Provider Number is required",]));
        // $this->add("insuranceUse", new PresenceOf(["message" => "Insurance Use is required",]));
        // $this->add("ubo4L51", new PresenceOf(["message" => "UB04 Locator 51 is required",]));
        // $this->add("ubo4L57", new PresenceOf(["message" => "UB04 Locator 57 is required",]));
        // $this->add("ubo4L81", new PresenceOf(["message" => "UB04 Locator 81CCa is required",]));
        // $this->add("hcfa17a", new PresenceOf(["message" => "HCFA 17a is required",]));
        // $this->add("hcfa24ij", new PresenceOf(["message" => "HCFA 24ij is required",]));
        // $this->add("hcfa32b", new PresenceOf(["message" => "HCFA 32b is required",]));
        // $this->add("hcfa33b", new PresenceOf(["message" => "HCFA 33b is required",]));
        // $this->add("contactPerson", new PresenceOf(["message" => "Contact Person is required",]));
        // $this->add("contactEmail", new PresenceOf(["message" => "Contact Email is required",]));
        // $this->add("phone", new PresenceOf(["message" => "Phone is required",]));
        // $this->add("fax", new PresenceOf(["message" => "Fax is required",]));
        // $this->add("var", new PresenceOf(["message" => "Visit Authorization Required is required",]));
        // $this->add("dam", new PresenceOf(["message" => "Display in Authorization Manager is required",]));
        // $this->add("dfi", new PresenceOf(["message" => "Default Fiscal Intermediary is required",]));

        //Email validity
        // $this->add("contactEmail", new Email(["message" => "The e-mail is not valid"]));



    }

}
