<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
use Models\Members;

class RequestResetPW extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("question1", new PresenceOf(["message" => "Security Question 1 is required",]));
        $this->add("question2", new PresenceOf(["message" => "Security Question 2 is required",]));
        $this->add("answer1", new PresenceOf(["message" => "Security Answer 1 is required.",]));
        $this->add("answer2", new PresenceOf(["message" => "Security Answer 2 is required.",]));
    }

}
