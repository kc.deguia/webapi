<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

use Models\Patient;

class ValPatient extends Validation
{
    public function initialize()
    {
        // Checking that must be required
        $this->add("lastName",              new PresenceOf(["message" => "Last Name is required",]));
        $this->add("firstName",             new PresenceOf(["message" => "First Name is required",]));
        $this->add("birthdate",             new PresenceOf(["message" => "Birthdate is required",]));
        $this->add("gender",                new PresenceOf(["message" => "Gender Name is required",]));
        // $this->add("ssNumber",              new PresenceOf(["message" => "SSN  is required",]));
        // $this->add("careProvide",           new PresenceOf(["message" => "Location where home health services were provided is required",]));
        // $this->add("address",               new PresenceOf(["message" => "Address is required",]));
        // $this->add("zipCode",               new PresenceOf(["message" => "Zip Code is required",]));
        // $this->add("state",                 new PresenceOf(["message" => "State is required",]));
        // $this->add("addPhone",              new PresenceOf(["message" => "Phone State is required",]));
        // $this->add("addEmail",              new PresenceOf(["message" => "Email Code is required",]));
        // $this->add("specifyCareType",       new PresenceOf(["message" => "Specify Care Type for HH-CAHPS Export Exclusion is required",]));

        //Email validity
        // $this->add("addEmail", new Email(["message" => "The e-mail is not valid"]));

        // // Check Email if Already Used
        // $this->add('email', new UniquenessValidator([
        //         'model' => '\Models\Patient',
        //         'attribute' => 'addEmail',
        //         'message' => 'Email already taken.'
        // ]));

        //Expiration Date
        $this->add("birthdate", new Date(["format" => "Y-m-d","message" => "The Expiration is not a valid date or format"]));

    }
}
