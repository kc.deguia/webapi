<?php

namespace Controllers;

#*** MODELS
use Models\Student;
use Models\Student_child;
use Models\Student_education;
use Models\Student_milback;
use Models\Student_register;

// Phalcon DI
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Manager as ModelsManager;
//Others
use Utilities\Guid\Guid;


class StudentController extends ControllerBase {

    protected $_studentData = [];

    public function studentAddAction(){

        $manager            = new TxManager();
        $transaction        = $manager->get();


        if($this->request->isPost()){

            $studenID = (new Guid())->GUID();

            #*** I. Personal Detail
            $_studentData['profile']            =  $this->request->getPost('profile', ['string','trim']);
            $_studentData['fname']              =  $this->request->getPost('fname', ['string','trim']);
            $_studentData['lname']              =  $this->request->getPost('lname', ['string','trim']);
            $_studentData['mname']              =  $this->request->getPost('mname', ['string','trim']);
            $_studentData['rank']               =  $this->request->getPost('rank', ['string','trim']);
            $_studentData['afpsn']              =  $this->request->getPost('afpsn', ['string','trim']);
            $_studentData['afpIdNo']            =  $this->request->getPost('afpIdNo', ['string','trim']);
            $_studentData['brSvcAfpos']         =  $this->request->getPost('brSvcAfpos', ['string','trim']);
            $_studentData['homeAddress']        =  $this->request->getPost('homeAddress', ['string','trim']);
            $_studentData['city']               =  $this->request->getPost('city', ['string','trim']);
            $_studentData['province']           =  $this->request->getPost('province', ['string','trim']);
            $_studentData['region']             =  $this->request->getPost('region', ['string','trim']);
            $_studentData['birthdate']          =  $this->request->getPost('birthdate', ['string','trim']);
            $_studentData['placeOfBirth']       =  $this->request->getPost('placeOfBirth', ['string','trim']);
            $_studentData['nickName']           =  $this->request->getPost('nickName', ['string','trim']);
            $_studentData['religion']           =  $this->request->getPost('religion', ['string','trim']);
            $_studentData['gsisNo']             =  $this->request->getPost('gsisNo', ['string','trim']);
            $_studentData['sssNo']              =  $this->request->getPost('sssNo', ['string','trim']);
            $_studentData['tinNo']              =  $this->request->getPost('tinNo', ['string','trim']);
            $_studentData['bloodType']          =  $this->request->getPost('bloodType', ['string','trim']);
            $_studentData['contactNo']          =  $this->request->getPost('contactNo', ['string','trim']);
            $_studentData['email']              =  $this->request->getPost('email', ['string','trim']);

            #*** II. Personal Characteristic
            $_studentData['sex']                =  $this->request->getPost('sex', ['string','trim']);
            $_studentData['age']                =  $this->request->getPost('age', ['string','trim']);
            $_studentData['weight']             =  $this->request->getPost('weight', ['string','trim']);
            $_studentData['height']             =  $this->request->getPost('height', ['string','trim']);
            $_studentData['built']              =  $this->request->getPost('built', ['string','trim']);
            $_studentData['complexion']         =  $this->request->getPost('complexion', ['string','trim']);
            $_studentData['eyeColor']           =  $this->request->getPost('eyeColor', ['string','trim']);
            $_studentData['hairColor']          =  $this->request->getPost('hairColor', ['string','trim']);
            $_studentData['scarMark']           =  $this->request->getPost('scarMark', ['string','trim']);
            $_studentData['stateHealth']        =  $this->request->getPost('stateHealth', ['string','trim']);
            $_studentData['phyMenDef']          =  $this->request->getPost('phyMenDef', ['string','trim']);
            $_studentData['recSerIll']          =  $this->request->getPost('recSerIll', ['string','trim']);


            #*** III. Marital History
            $_studentData['maritalStatus']      =  $this->request->getPost('maritalStatus', ['string','trim']);
            $_studentData['spouseName']         =  $this->request->getPost('spouseName', ['string','trim']);
            $_studentData['spouseBirthdate']    =  $this->request->getPost('spouseBirthdate', ['string','trim']);
            $_studentData['marriageDate']       =  $this->request->getPost('marriageDate', ['string','trim']);
            $_studentData['spouseOccupation']   =  $this->request->getPost('spouseOccupation', ['string','trim']);
            $_studentData['spouseCitizenship']  =  $this->request->getPost('spouseCitizenship', ['string','trim']);
            $_studentData['ifNaturalized']      =  $this->request->getPost('ifNaturalized', ['string','trim']);


            #*** IV. Educational Background
            $_studentData['civilSrvc']          =  $this->request->getPost('civilSrvc', ['string','trim']);

            #*** V. Military Backgroun
            $_studentData['dateEntMilSvc']      =  $this->request->getPost('dateEntMilSvc', ['string','trim']);
            $_studentData['dateEnlisted']       =  $this->request->getPost('dateEnlisted', ['string','trim']);
            $_studentData['dateCommision']      =  $this->request->getPost('dateCommision', ['string','trim']);
            $_studentData['sourceComission']    =  $this->request->getPost('sourceComission', ['string','trim']);
            $_studentData['dateCad']            =  $this->request->getPost('dateCad', ['string','trim']);
            $_studentData['sepMilSvc']          =  $this->request->getPost('sepMilSvc', ['string','trim']);
            $_studentData['sateNature']         =  $this->request->getPost('sateNature', ['string','trim']);


            #*** VI. Family Picture
            $_studentData['familyPic']          =  $this->request->getPost('familyPic', ['string','trim']);



            #*** SAVE_DATA
            try {

                if($this->request->getPost('studentid')){
                    $savetoDb = Student::findFirst([
                        "conditions"    => "studentid = :id:",
                        "bind"          => array("id" => $this->request->getPost('studentid'))
                    ]);

                    if($savetoDb){
                        $successMsg = "Data Successfuly updated!";
                        $studenID   =  $this->request->getPost('studentid');
                        $_studentData['dateedited']         =  date('Y-m-d H:i:s');
                    }else{
                        $transaction->rollback('Something went wrong , Student not found');
                    }

                }else{
                    $savetoDb  =   new Student();
                    $_studentData['studentid']          =  $studenID;
                    $_studentData['datecreated']        =  date('Y-m-d H:i:s');
                    $_studentData['dateedited']         =  date('Y-m-d H:i:s');
                    $successMsg = "Data Successfuly saved!";
                }

                $savetoDb->setTransaction($transaction);
                $savetoDb->assign($_studentData);

                if(!$savetoDb->save()){
                    $this->errors = $this->errorObjectToArray($savetoDb->getMessages());
                    $transaction->rollback('Something went wrong unable to save new student record.');
                }else{

                    #*** CHILDREN
                    #================================================================
                    foreach ($this->request->getPost('children') as $key => $value) {

                        $saveChildData = array(
                            "studentid"     => $studenID,
                            "firstName"     => $value['firstName'],
                            "middleName"    => $value['middleName'],
                            "lastName"      => $value['lastName'],
                            "birthDate"     => $value['birthDate'],
                            "address"       => $value['address'],
                            "dateedited"    => date('Y-m-d H:i:s')
                        );

                        if(array_key_exists('childrenid', $value)){
                            $saveChild = Student_child::findFirst([
                                "conditions"    => "childrenid = :id:",
                                "bind"          => array("id" => $value['childrenid'])
                            ]);

                        }else{
                            $saveChild  =   new Student_child();
                            $saveChildData['childrenid'] = (new Guid())->GUID();
                            $saveChildData['datecreated'] = date('Y-m-d H:i:s');
                        }

                        $saveChild->setTransaction($transaction);
                        $saveChild->assign($saveChildData);
                        if(!$saveChild->save()){
                            $this->errors = $this->errorObjectToArray($savetoDb->getMessages());
                            $transaction->rollback('Something went wrong unable to save new student record.');
                            break;
                        }
                    }

                    #*** EDUCATIONAL BACKGROUND
                    #================================================================
                    $educAttainment = array('elementary', 'highSchool', 'college', 'postGraduate', 'otherSchoolAttended');

                    foreach ($educAttainment as $k => $v) {
                        $loop = true;
                        if($this->request->getPost($v)){
                            $dataInfo = $this->request->getPost($v);

                            foreach ($dataInfo as $key => $value) {
                                $schooldData = array(
                                    "studentid"         => $studenID,
                                    "attainment"        => $v,
                                    "schoolName"        => $value['schoolName'],
                                    "location"          => $value['location'],
                                    "dateOfAttendance"  => $value['dateOfAttendance'],
                                    "dateedited"    => date('Y-m-d H:i:s')
                                );


                                if(array_key_exists('educationid', $value)){
                                    $saveSchool = Student_education::findFirst([
                                        "conditions"    => "educationid = :id:",
                                        "bind"          => array("id" => $value['educationid'])
                                    ]);

                                }else{
                                    $saveSchool  =   new Student_education();
                                    $schooldData['educationid'] = (new Guid())->GUID();
                                    $schooldData['datecreated'] = date('Y-m-d H:i:s');
                                }

                                $saveSchool->setTransaction($transaction);
                                $saveSchool->assign($schooldData);
                                if(!$saveSchool->save()){
                                    $this->errors = $this->errorObjectToArray($savetoDb->getMessages());
                                    $transaction->rollback('Something went wrong unable to save new student record.');
                                    $loop = false;
                                    break;
                                }
                            }
                        }
                        if(!$loop){
                            break;
                        };
                    }

                    #*** Military BACKGROUND
                    #================================================================
                    $milBackground = array('importUnitASsignment', 'milSchoolAttended');

                    foreach ($milBackground as $k => $v) {
                        $loop = true;
                        if($this->request->getPost($v)){
                            $dataInfo = $this->request->getPost($v);

                            foreach ($dataInfo as $key => $value) {
                                $schooldData = array(
                                    "studentid"         => $studenID,
                                    "milBackground"     => $v,
                                    "designation"       => $value['designation'],
                                    "inclusiveDate"     => $value['inclusiveDate'],
                                    "unit"              => $value['unit'],
                                    "dateedited"    => date('Y-m-d H:i:s')
                                );


                                if(array_key_exists('milbackid', $value)){
                                    $saveSchool = Student_milback::findFirst([
                                        "conditions"    => "milbackid = :id:",
                                        "bind"          => array("id" => $value['milbackid'])
                                    ]);

                                }else{
                                    $saveSchool  =   new Student_milback();
                                    $schooldData['milbackid'] = (new Guid())->GUID();
                                    $schooldData['datecreated'] = date('Y-m-d H:i:s');
                                }

                                $saveSchool->setTransaction($transaction);
                                $saveSchool->assign($schooldData);
                                if(!$saveSchool->save()){
                                    $this->errors = $this->errorObjectToArray($savetoDb->getMessages());
                                    $transaction->rollback('Something went wrong unable to save new student record.');
                                    $loop = false;
                                    break;
                                }
                            }
                        }
                        if(!$loop){
                            break;
                        };
                    }
                }

                $transaction->commit();
                return array(
                    "success" => $successMsg,
                    "statuscode" => 200,
                    "pk" => $studenID,
                );
            }catch(TxFailed $e){
                $this->ErrorMessage->thrower(406, $e->getMessage(), "Saving Errors", "EXREF101", $this->errors);
            }
        }
    }

    public function studentListAction($page = 0, $count, $keyword){

        if(empty($count)){
            $count=10;
        }

        if(empty($keyword)){
            $keyword=null;
        }

        $result = Student::dataList($page, $count, $keyword);

        return $result;

    }

    public function studentInfoAction($studentId){
        $manager            = new TxManager();
        $transaction        = $manager->get();
        try {
            $data = Student::findFirst([
                "conditions"    => "studentid = :id:",
                "bind"          => array("id" => $studentId)
            ]);
            if(!$data){
                $transaction->rollback('Unable to Fetch Data. Student Record Does Not Exist!');
            }else{
                $data = $data->toArray();
                #*** CHILDREN
                #================================================================
                $thisChildren = Student_child::find([
                    "conditions"    => "studentid = :id:",
                    "bind"          => array("id" => $studentId)
                ]);
                $data['children'] =  $thisChildren->toArray();

                #*** EDUCATIONAL BACKGROUND
                #================================================================
                $educAttainment = array('elementary', 'highSchool', 'college', 'postGraduate', 'otherSchoolAttended');

                foreach ($educAttainment as $k => $v) {
                    $educational = Student_education::find([
                        "conditions"    => "studentid = :id: AND attainment= :attainment: ",
                        "bind"          => array("id" => $studentId, "attainment"=> $v)
                    ]);

                    $data[$v] = $educational->toArray();
                }

                #*** Military BACKGROUND
                #================================================================
                $milBackground = array('importUnitASsignment', 'milSchoolAttended');

                foreach ($milBackground as $k => $v) {
                    $educational = Student_milback::find([
                        "conditions"    => "studentid = :id: AND milBackground= :attainment: ",
                        "bind"          => array("id" => $studentId, "attainment"=> $v)
                    ]);

                    $data[$v] = $educational->toArray();
                }

            }
            $transaction->commit();
            return $data;
        }catch(TxFailed $e){
            $this->ErrorMessage->thrower(406,$e->getMessage(),"Delete Record Error","HOSP105",$this->errors);
        }
    }




    public function registerListAction($page = 0, $count, $keyword){
        
        if(empty($count)){
            $count=10;
        }

        if(empty($keyword)){
            $keyword=null;
        }

        return Student_register::dataList($page, $count, $keyword);

    }


    public function regInfoAction($regid){
        $manager            = new TxManager();
        $transaction        = $manager->get();
        try {
            $data = Student_register::findFirst([
                "conditions"    => "pkregister = :id:",
                "bind"          => array("id" => $regid)
            ]);
            if(!$data){
                $transaction->rollback('Unable to Fetch Data. Student Record Does Not Exist!');
            }

            $transaction->commit();
            return $data;
        }catch(TxFailed $e){
            $this->ErrorMessage->thrower(406,$e->getMessage(),"Delete Record Error","HOSP105",$this->errors);
        }
    }

}
