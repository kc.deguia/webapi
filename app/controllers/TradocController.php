<?php

namespace Controllers;

use Models\Tradoc_register;

// Phalcon DI
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Manager as ModelsManager;
//Others
use Utilities\Guid\Guid;
use RequestValidator\Hospital as CMValidator;


class TradocController extends ControllerBase{


    public function registerAction(){

        if($this->request->getPost()){
    
            try {

                $transaction        = (new TxManager())->get();


                $saveDataInfo = array(
                    "pkregister"    => (new Guid())->GUID(),
                    "fname"         => $this->request->getPost('fname'),
                    "lname"         => $this->request->getPost('lname'),
                    "mname"         => $this->request->getPost('mname'),
                    "rank"          => $this->request->getPost('rank'),
                    "afpSn"         => $this->request->getPost('afpSn'),
                    "afpIdno"       => $this->request->getPost('afpIdno'),
                    "dateOfBirth"   => $this->request->getPost('dateOfBirth'),
                    "placeOfBirth"  => $this->request->getPost('placeOfBirth'),
                    "religion"      => $this->request->getPost('religion'),
                    "contact"       => $this->request->getPost('contact'),
                    "email"         => $this->request->getPost('email'),
                    "declareInfo"   => $this->request->getPost('declareInfo'),
                    "dateCreated"   => date('Y-m-d H:i:s'),
                    "dateUpdated"   => date('Y-m-d H:i:s'),
                );

                $savetoDb = new Tradoc_register();
            
                $savetoDb->setTransaction($transaction);
                $savetoDb->assign($saveDataInfo);
                if(!$savetoDb->save()){
                    $this->errors = $this->errorObjectToArray($savetoDb->getMessages());
                    $transaction->rollback('Cannot save Hospital.');
                }

                $transaction->commit();

                return array(
                    "success" => "Data Successfuly saved!",
                    "statuscode" => 200
                );
                
            }
            catch (\Exception $e) {
                $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Saving Errors",
                    "MEM002",
                    $this->errors
                );
            }   
        }
    }

    public function listRegister(){
        return Tradoc_register::find();
    }

}
