<?php

/**
 * @SWG\Swagger(
 *     schemes={"http", "https"},
 *     host="gn.api",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="HealthCare API Framework",
 *         description="Just a API Documentation for the Geeksnest API Framework",
 *         @SWG\Contact(
 *             email="efren.bautista.jr@geeksnest.com"
 *         )
*     )
 * )
 */

namespace Controllers;

use Constants\DocStatus;

use Template\Model\TemplateModel;
use Models\Patienttimeline;


class ControllerBase extends \Phalcon\DI\Injectable {

    private $_redis;

    public function __construct()
    {
        $di = \Phalcon\DI::getDefault();
        $this->setDI($di);
        $this->clientRequest  = $this->di->get("request")->getJsonRawBody();
    }

    public function validationResponseError($validation, $data, $validators = null, $code = null){
        if($validators){
            foreach ($validators as $key => $val){
                $validation->add($key, $val);
            }
        }

        $messages = $validation->validate($data);

        if (count($messages)) {
            $this->ErrorMessage->thrower(
                406,
                "Validation Errors",
                "Validation Errors",
                $code,
                $this->errorObjectToArray($messages)
            );
        }
    }

    public function modelsManager($phql) {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $result = $app->modelsManager->executeQuery($phql);
    }

    public function createBuilder() {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $app->modelsManager->createBuilder();
    }

    public function getConfig(){
        $config = include __DIR__ . "/../config/config.php";

        return $config;
    }

    public function customQuery($phql, $params = []) {
      // $dbbnb = \Phalcon\DI::getDefault()->get('db');
      // $stmt = $dbbnb->prepare($phql);
      // $stmt->execute($params);
      // $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      // return $result;
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('db');
        $stmt = $db->prepare($phql);
        if(count($params) != 0){
            foreach ($params as $param) {
                if($param['type'] == 'int'){
                    $stmt->bindParam($param['keyword'], $param['value'], \PDO::PARAM_INT);
                } else {
                    $stmt->bindParam($param['keyword'], $param['value'], \PDO::PARAM_STR);
                }
            }
        }

        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public static function customQueryFirst($phql, $params = []){
        $di = \Phalcon\DI::getDefault();
        $dbbnb = $di->get('db');
        $stmt = $dbbnb->prepare($phql);

        if(count($params) != 0){
            foreach ($params as $params) {
                if($params['type'] == 'int'){
                    $stmt->bindParam($params['keyword'], $params['value'], \PDO::PARAM_INT);
                }
                else{
                    $stmt->bindParam($params['keyword'], $params['value'], \PDO::PARAM_STR);
                }
            }
        }
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function gethashkey(){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $app->config->hashkey;
    }

    public function check_in_range($start_date, $end_date, $date_from_user)
    {
      $start_ts = strtotime($start_date);
      $end_ts = strtotime($end_date);
      $user_ts = strtotime($date_from_user);
      return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    public function geterrormessage($datavalue){
        $errors = array();
        foreach ($datavalue->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        return $errors;
    }

    public function savetoDb($savetoDb){
      function getError($err){
        $errors = array();
        foreach ($err->getMessages() as $message) {
          $errors[] = $message->getMessage();
        }
        return $errors;
      }
      $success = array('success' => 'Data Successfuly saved!', 'statuscode' => 200 );
      $error   = array('error'=> 'Something went wrong!', 'statuscode' => 400 , 'data'=>getError($savetoDb), );
      return ($savetoDb->save() ? $success : $error);
    }

    /*
    * OASIS Checkbox ~ slctLoop function
    * @param1 = Oasis ID
    * @param2 = Foreign key table name
    * @param3 = Array (Selected Checkboxes)
    * @param4 = Table Name
    * @param5 = Column Name
    */
    public function slctLoop($id,$colname, $a, $table, $col){
        $db = \Phalcon\DI::getDefault()->get('db');
        // DELETE ALL DATA RELATED BY OASIS ID
        $queryDelete = 'DELETE  FROM '.$table.' WHERE '.$colname.'="'.$id.'"';
        $delete = $db->prepare($queryDelete);
        $delete->execute();

        $loopCount = 1;
        $arrayQuery='';
        if($a && count($a)!=0){
            foreach ($a as $key => $value){
                $comma = $loopCount <  count($a)? ',':'';
                $arrayQuery.='("'.$id.'", "'.$value.'")'.$comma;
                $loopCount++;
            }

            // INSERT SELECTED DATA
            $queryInsert ='INSERT INTO '.$table.' ('.$colname.', '.$col.') VALUES '.$arrayQuery;
            $insert = $db->prepare($queryInsert);
            $insert->execute();
        }

        return $queryInsert ? true : false;
    }

    public function toColumn($name, $type){
        return array("data"=>$name, "type"=>$type);
    }

    public function alterQuery($arrayData, $afterCol, $alterTable, $tableName, $tableModel){

        $db             = \Phalcon\DI::getDefault()->get('db');
        $metaDataMem    = new \Phalcon\Mvc\Model\MetaData\Memory();
        $metaData       = $metaDataMem->getAttributes($tableModel);

        $saveArray = array();

        $after     = $afterCol;

        foreach($arrayData as $key => $value){
            $saveArray[$key] = $value['data'];

            if($alterTable){
                // Check if Column name is already existed in the table
                if(in_array($key, $metaData)){
                    // Update column
                    $alt = "CHANGE ".$key." ".$key." ".$value['type'];
                }else{
                    // Add new column
                    $alt = "ADD ".$key." ".$value['type']." AFTER ".$after;
                }

                $query = "ALTER TABLE ".$tableName." ".$alt."";
                $stmt = $db->prepare($query);
                $stmt->execute();
                $after = $key;
            }
        }

        return $saveArray ;
    }


    public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    public function MRNumber() {
        $alphabet = "0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    private function redisInit(){

        $app = $this->di->get('application');

        $redis = new \Redis();
        $redis->connect($app->config->redis->host, $app->config->redis->port);

        $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);   // don't serialize data
        $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);    // use built-in serialize/unserialize
        $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_IGBINARY);   // use igBinary serialize/unserialize

        $redis->setOption(\Redis::OPT_PREFIX, $app->config->redis->sessionkey); // use custom prefix on all keys
        // Will set the key, if it doesn't exist, with a ttl of 10 seconds
        //$redis->set('key', 'value', Array('nx', 'ex'=>60));
        //$now = time();
        $this->_redis = $redis;
        //$redis->expireAt('key', $now + 60);
        //var_dump($redis->ttl('key'));
    }

    public function checkRedis(){
        $this->redisInit();
        return $this->_redis->ping();
    }

    public function redisSet($key, $value){
        $this->redisInit();
        return $this->_redis->set($key, $value);
    }

    public function redisSAdd($key, $value){
        $this->redisInit();
        return $this->_redis->sadd($key, $value);
    }

    public function redisSMembers($key){
        $this->redisInit();
        return $this->_redis->smembers($key);
    }

    public function redisGet($key){
        $this->redisInit();
        return $this->_redis->get($key);
    }

    public function redisSRem($key, $value){
        $this->redisInit();
        return $this->_redis->srem($key, $value);
    }

    // public function redisGetAll(){
    //     return $this->_redis->get();
    // }

    public function errorObjectToArray($messages){

        $errormessage = [];
        foreach ($messages as $message) {
            $errormessage[] = [
                $message->getField() => $message->getMessage()
            ];
        }
        return $errormessage;
    }

    public static function getOasisData($oasis, $colName, $colValue, $subTable){
        function subModel($model, $col, $colKey , $id){
            $result =  $model::find([ "columns"=>"".$col."", "conditions"=>"".$colKey." = :id: AND status = '1'", "bind" => array("id" => $id)])->toArray();
            return array_column($result, $col);
        }
        foreach ($subTable as $key => $value){
            if($value['bol']){
                $oasis[$value['col']] = subModel($value['model'], $value['col'], $colName, $colValue);
            }
        }
        return $oasis;
    }

    public function convert_discipline_to_full($discipline) {
        switch ($discipline) {
            case 'PT':
                $discipline = 'PHYSICAL THERAPHY';
                break;
            case 'ST':
                $discipline = 'SPEECH THERAPHY';
                break;
            case 'OT':
                $discipline = 'OCCUPATIONAL THERAPHY';
                break;
            default:
                $discipline = 'SKILLED NURSE';
                break;
        }

        return $discipline;
    }


    public function savingDocStatus($status, $type, $foreignid, $idpatient, $periodid=null){

        $ds = new \ReflectionClass('Constants\DocStatus');
        $loc = $ds->getConstant('LOC');
        $rqa = $ds->getConstant('READYFORQA');

        if(strpos($type, '-') != false) {
            $str = explode("-", $type);
            $sub = strtolower($str[1]);
            $title = $ds->getConstant($str[0])[$sub];
            $loc = $loc[$str[0]][$sub];
        }else{
            $title = $ds->getConstant($type);
            $loc = $loc[$type];
        }

        $findStat = \Models\Patientcaredocstatus::findFirst([
            " foreignid = ?0 ",
            "bind" => [$foreignid]
        ]);



        $docstatus = (!$findStat ? new \Models\Patientcaredocstatus() : $findStat);
        $docstatus->title = $title;

        $sequence = DocStatus::SEQUENCE;
        $max_sequence = DocStatus::MAX_SEQUENCE;

        if($findStat){
            if($sequence[$findStat->status] < $sequence[$status]){
                $docstatus->status = $status;
                $docstatus->statuscode = str_replace(" ", "-", strtolower($status));
            }
            // separated for testing purpose
            if($sequence[$findStat->status] == $max_sequence) {
                $docstatus->status = $status;
                $docstatus->statuscode = str_replace(" ", "-", strtolower($status));
            }
        } else {
            $docstatus->status = $status;
            $docstatus->statuscode = str_replace(" ", "-", strtolower($status));
        }


        $docstatus->typeofnotes = $title;
        $docstatus->foreignid = $foreignid;
        $docstatus->patientid = $idpatient;
        $docstatus->periodid = $periodid;
        $docstatus->location = $loc;
        if(!$docstatus->save()){
            throw new \Micro\Exceptions\HTTPExceptions(
                "Document Status Saving Error",
                400,
                array(
                    'dev' => "Document Status Error.",
                    'internalCode' => "AUTH0002",
                    'more' => "Authorization not save!"
                ));
        }

        return $docstatus->toArray();
    }

    public function img_to_dataurl($path_to_image) {
        // $type = pathinfo($path_to_image, PATHINFO_EXTENSION);
        $type = 'png';
        $image = file_get_contents($path_to_image);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($image);

        return $base64;
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function get_physician_name($idphysician) {
        $name = null;

        $physician = \Models\Physician::findFirst([
            "columns" => "CONCAT(firstName, ' ', lastName) AS name",
            "idphysician = :idphysician:",
            "bind" => [
                "idphysician" => $idphysician
            ]
        ]);

        if($physician) {
            $name = $physician->name;
        }

        return $name;
    }

    public function verifyEsignCode($code) {

        if(!empty($code)) {
            $member = \Models\Members::findFirst([
                "columns" => "CONCAT(firstname,lastname,',',credential) AS name, CONCAT(firstname,' ',lastname) AS fullname, memberid, esign_code",
                "memberid = :memberid:",
                "bind" => [
                    "memberid" => $this->User->getUser()->id
                ]
            ]);

            if($member) {
                if($this->security->checkHash($code, $member->esign_code)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function unset_invalid_dates(&$data, $keys = array( "orderDate", "sentDate", "receiveDate" )) {
        $invalid = "Invalid date";

        foreach ($keys as $key => $value) {
            if($data === $invalid) {
                unset($data[$value]);
            }
        }
    }


    public function createTable($tableName, $tableCol){
        $db  = \Phalcon\DI::getDefault()->get('db');

        $stmt = $db->prepare("SHOW TABLES LIKE '".$tableName."'");
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        if(!$result){

            echo "test";
            $ifPrimeKey = '';
            try {
                $queryStart = "CREATE TABLE `".$tableName."` (";
                $queryCol   = "";
                $queryEnd   = ") ENGINE = InnoDB";

                $exicute  = true;
                $incIndex = 0;
                foreach ($tableCol as $key => $value) {
                    $incIndex++;

                    $comma = $incIndex < count($tableCol) ? ',':'';

                    //NOTE : OTHER VALIDATION FOR EACH COLUMN WILL BE ADDED DURING THIS FUNCTIONS DEVELOPMENT
                    if($value['type'] == "CHAR"){
                        if(! array_key_exists("length",$value)){
                            $this->ErrorMessage->thrower(406,"",$key." length is required","SQLCS0001","Length of the type is Needed");
                            $exicute = false;
                            break;
                        }else{
                            $value['type'] = "CHAR(".$value['length'].")";
                        }
                    }

                    if(array_key_exists("isNull", $value)){
                        $isNull = $value['isNull']  ? "NOT NULL" : "NULL";
                    }else{
                        $isNull = "NULL";
                    }

                    if(array_key_exists("pk", $value)){
                        $ifPrimeKey = $value['pk']  ? ", PRIMARY KEY (`".$key."`)" : '';
                    }
                    $queryCol.= "`".$key."` ".$value['type']." ".$isNull.$comma." ";

                }

                if($exicute){
                    $query = $queryStart.$queryCol.$ifPrimeKey.$queryEnd;


                    $exec  = $db->prepare($query);
                    if($exec->execute()){

                        // GENERATE FILE MODEL ACCORDING TO THE CREATED TABLE
                        $modelDir = str_replace('controllers','', dirname(__FILE__))."models/";
                        $newModel = $modelDir.ucfirst($tableName).".php";
                        $newModelContent = TemplateModel::modelDefault(ucfirst($tableName));
                        if (file_put_contents($newModel, $newModelContent) !== false) {
                            return array("success"=> "File created (" . basename($newModel) . ")");
                        } else {
                            return array("error"=> "Cannot create file (" . basename($newModel) . ")");
                        }

                    }else{
                        return array("error"=> "Unable To Create New Table");
                    }
                }
            } catch (Exception $e) {
                var_dump($e->getMessage());
            }


        }else{
            return false;
            // $this->ErrorMessage->thrower(406,"","Table Already Exist","SQLCS0002","Table Already Exist");
        }

    }

    public function insertTotable($query, $tableData){
        $db  = \Phalcon\DI::getDefault()->get('db');
        try {
            $insert = $db->prepare($query.$tableData);
            if($insert->execute()){
                return true;
            }else{
                $this->ErrorMessage->thrower(406,"","Unable Set Data","SQLCS0001","Unable Set Data");
            }

        } catch (Exception $e) {
            $this->ErrorMessage->thrower(406,$e->getMessage(),"Inserting Error","SQLCS0002", "errors");
        }
    }


    public function importCSV($dir, $table){
        $db  = \Phalcon\DI::getDefault()->get('db');

        $disModel = "Models\\".ucfirst($table);

        $metaDataMem    = new \Phalcon\Mvc\Model\MetaData\Memory();
        $metaData       = $metaDataMem->getAttributes(new $disModel());

        $incIndex = 0;
        $tblColumn = "";
        foreach ($metaData as $key => $value) {
            $incIndex++;
            $comma = $incIndex < count($metaData) ? ',':'';
            $tblColumn.=$value.$comma;
        }

        try {

            $query = "
            LOAD DATA LOCAL INFILE '".$dir."'
            INTO TABLE ".$table."
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            ";


            $db->query($query);

            $insert = $db->prepare($query);

            var_dump($insert->execute());

            if($insert->execute()){
                echo "importCSV";
            }else{
                var_dump($insert->execute()->getMessage());

            }

        } catch (Exception $e) {
            $this->ErrorMessage->thrower(406,$e->getMessage(),"Importing CSV Error","SQLCS0003", "errors");
        }
    }

    public function tblVersion(){
        $file = str_replace('controllers','', dirname(__FILE__))."constants/TableModelVersions.txt";
        return json_decode(file_get_contents($file), true);
    }


    public function timelinelog($params){


        $addlog = new Patienttimeline();

        $addlog->id             = $params['id'];
        $addlog->user           = $params['user'];
        $addlog->module         = $params['module'];
        $addlog->moduleid       = $params['moduleid'];
        $addlog->patientid      = $params['patientid'];
        $addlog->action         = $params['action'];
        $addlog->datecreated    = date('Y-m-d H:i:s');

        if(!$addlog->save()){
            return array(
                'status' => 'error',
                'id' => $params['id']
                );
        }
        else{
            return array(
                'status' => 'success'
                );
        }

    }

    public function customPaginator($data, $limit, $page) {
        $total_items = count($data);
        $offset = ($page * $limit) - $limit;

        $total_pages = ceil($total_items  / $limit);
        if($total_pages < $page) {
            $page--;
            $offset = ($page * $limit) - $limit;
        }

        $before = $page > 1 ? $page - 1 : 1;
        $next = $page < $total_pages ? $page + 1 : $total_pages;

        $paginator = [
            "items" => array_slice($data, $offset, $limit),
            'first' => 1,
            'before' => $before,
            'current' => $page,
            'last' => $total_pages,
            'next' => $next,
            'totalPages' => $total_pages,
            'totalItems' => $total_items,
            'limit' => $limit
        ];

        return $paginator;
    }

}
