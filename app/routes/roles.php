<?php


/**
 * @author GEEKSNEST fork from Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.

 */
return [
    "prefix" => "/roles",
    "handler" => 'Controllers\RolesController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'get',
            'route' => '/endpoints',
            'function' => 'endpoints',
            'authentication' => FALSE
        ]
    ]
];
