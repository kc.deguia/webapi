<?php

/**
 * @author GEEKSNEST
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */

return [
    "prefix" => "/members",
    "handler" => 'Controllers\MembersController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'post',
            'route' => '/memberlogin',
            'function' => 'memberLogin',
            'authentication' => false
        ],
        [
            'method' => 'post',
            'route' => '/refreshtoken',
            'function' => 'refreshtoken',
            'authentication' => false
        ],
        [
            'method' => 'get',
            'route' => '/recalibrate_roles',
            'function' => 'initializeRoles',
            'authentication' => true,
            'resource' => 'sys-000'
        ],
        [
            'method' => 'get',
            'route' => '/user/roles/{memberid}',
            'function' => 'generateUserCacheAcl',
            'authentication' => true,
            'resource' => 'usr-art'
        ],

        [
            'method' => 'post',
            'route' => '/addmember',
            'function' => 'addMember',
            'authentication' => false, // DEFAULT TRUE
            'resource' => 'usr-ae'
        ],
        [
            'method' => 'post',
            'route' => '/checkusername',
            'function' => 'checkusername',
            'authentication' => true,
            'resource' => 'usr-ae'
        ],
        [
            'method' => 'get',
            'route' => '/by/status/{status}',
            'function' => 'getMembersByStatus',
            'authentication' => true,
            'resource' => "sys-000"
        ],
        [
            'method' => 'get',
            'route' => '/list/{page}/{count}/{keyword}',
            'function' => 'memberList',
            'authentication' => false,
            'resource' => ['usr-aui','usr-del', 'usr-rup', 'usr-ae']
        ],
        [
            'method' => 'delete',
            'route' => '/member/{id}',
            'function' => 'deleteMember',
            'authentication' => true,
            'resource' => 'usr-del'
        ],
        [
            'method' => 'get',
            'route' => '/loadmemberbyid/{memberid}',
            'function' => 'loadMemberbyid',
            'authentication' => true,
            'resource' => ['usr-aui', 'usr-ae']
        ],
        [
            'method' => 'put',
            'route' => '/member',
            'function' => 'editMember',
            'authentication' => true,
            'resource' => 'usr-ae'
        ],
        [
            'method' => 'post',
            'route' => '/saveroletemplate',
            'function' => 'saveroletemplate',
            'authentication' => true,
            'resource' => ['usr-art']
        ],
        [
            'method' => 'put',
            'route' => '/roletemplate',
            'function' => 'updateroletemplate',
            'authentication' => true,
            'resource' => ['usr-art']
        ],
        [
            'method' => 'get',
            'route' => '/roletemplateitems/{id}',
            'function' => 'loadroletemplatebyid',
            'authentication' => true,
            'resource' => ['usr-art', 'usr-ae']
        ],
        [
            'method' => 'get',
            'route' => '/roletemplates/{page}/{count}/{keyword}',
            'function' => 'roletemplatelist',
            'authentication' => true,
            'resource' => ['usr-art', 'usr-ae']
        ],
        [
            'method' => 'get',
            'route' => '/roletemplate',
            'function' => 'getroletemplate',
            'authentication' => true,
            'resource' => ['usr-art', 'usr-ae']
        ],
        [
            'method' => 'delete',
            'route' => '/roletemplates/{id}',
            'function' => 'deleteroletemplate',
            'authentication' => true,
            'resource' => 'usr-art'
        ],
        [
            'method' => 'post',
            'route' => '/editroletemplate',
            'function' => 'editroletemplate',
            'authentication' => true,
            'resource' => 'usr-ae'
        ],
        [
            'method' => 'get',
            'route' => '/getMemberList',
            'function' => 'getMemberList',
            'authentication' => true,
            'resource' => ['pm-apr', 'pm-apg', 'usr-del', 'usr-aui']
        ],
        [
            'method' => 'get',
            'route' => '/profile',
            'function' => 'loadprofile',
            'authentication' => true,
            'resource' => 'sys-000'
        ],
        [
            'method' => 'put',
            'route' => '/profile',
            'function' => 'updateprofile',
            'authentication' => true,
            'resource' => 'sys-pre'
        ],
        [
            'method' => 'post',
            'route' => '/changepassword',
            'function' => 'changepassword',
            'authentication' => true,
            'resource' => 'sys-pre'
        ],
        [
            'method' => 'post',
            'route' => '/account/security',
            'function' => 'setSecurity',
            'authentication' => true,
            'resource' => 'sys-as'
        ],
        [
            'method' => 'post',
            'route' => '/forgotpassword/identify',
            'function' => 'fpCheckIdentity',
            'authentication' => false
        ],
        [
            'method' => 'get',
            'route' => '/forgotpassword/security_questions',
            'function' => 'fpGetQuestions',
            'authentication' => false
        ],
        [
            'method' => 'post',
            'route' => '/forgotpassword/reset_request',
            'function' => 'fpResetRequest',
            'authentication' => false
        ],
        [
            'method' => 'post',
            'route' => '/forgotpassword/new_password',
            'function' => 'fpPasswordReset',
            'authentication' => false
        ],
        [
            'method' => 'get',
            'route' => '/loginstatus/{memberid}',
            'function' => 'loginstatus',
            'authentication' => true,
            'resource' => 'sys-000'
        ],
        [
            'method' => 'get',
            'route' => '/security/question',
            'function' => 'getsecurityquestion',
            'authentication' => true,
            'resource' => 'sys-as'
        ],
        [
            'method' => 'get',
            'route' => '/getroleitems',
            'function' => 'getRoleitemsCb',
            'authentication' => true,
            'resource' => 'sys-000'
        ],
        [
            'method' => 'post',
            'route' => '/checkemail',
            'function' => 'checkEmail',
            'authentication' => true,
            'resource' => 'sys-000'
        ],
        [
            'method' => 'post',
            'route' => '/checkusername',
            'function' => 'checkUsername',
            'authentication' => true,
            'resource' => 'sys-000'
        ],
        [
            'method' => 'post',
            'route' => '/checkssn',
            'function' => 'checkSSN',
            'authentication' => true,
            'resource' => 'usr-ae'
        ],
        [
            'method' => 'post',
            'route' => '/admin/passwordreset',
            'function' => 'passwordReset',
            'authentication' => true,
            'resource' => 'usr-rup'
        ],

        [
            'method' => 'post',
            'route' => '/password',
            'function' => 'changePassword',
            'authentication' => true,
            'resource' => 'sys-000'
        ],

        [
            'method' => 'put',
            'route' => '/esign',
            'function' => 'esignUpdate',
            'authentication' => true,
            'resource' => 'usr-esi'
        ],

        [
            'method' => 'post',
            'route' => '/title',
            'function' => 'loadMemberByTitle',
            'authentication' => true,
            'resource' => 'sys-000'
        ],

        [
            'method' => 'post',
            'route' => '/esign',
            'function' => 'verifyEsignCode',
            'authentication' => true,
            'resource' => 'usr-esi'
        ],

        [
            'method' => 'post',
            'route' => '/esign/v2',
            'function' => 'set_document_esign',
            'authentication' => true,
            'resource' => 'usr-esi'
        ],
        [
            'method' => 'get',
            'route' => '/notification-care-coordination',
            'function' => 'get_notification_care_coordinates',
            'authentication' => true,
            'resource' => 'usr-esi'
        ]

        // [
        //     'method' => 'post',
        //     'route' => '/memberuploadfile',
        //     'function' => 'memberuploadfile',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],
        // [
        //     'method' => 'post',
        //     'route' => '/allmemberfilelist',
        //     'function' => 'allmemberfilelist',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],
        // [
        //     'method' => 'post',
        //     'route' => '/memberfilelist',
        //     'function' => 'memberfilelist',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],
        // [
        //     'method' => 'post',
        //     'route' => '/memberfiledelete',
        //     'function' => 'memberfiledelete',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],
        // [
        //     'method' => 'post',
        //     'route' => '/memberedituploadfile',
        //     'function' => 'memberedituploadfile',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],
        // [
        //     'method' => 'post',
        //     'route' => '/membercreatefolder',
        //     'function' => 'membercreatefolder',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],
        // [
        //     'method' => 'post',
        //     'route' => '/memberdetelefolder',
        //     'function' =>  'memberdetelefolder',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],
        // [
        //     'method' => 'post',
        //     'route' => '/editfolder',
        //     'function' => 'editfolder',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],
        // [
        //     'method' => 'post',
        //     'route' => '/memberdownloadfile',
        //     'function' => 'memberdownloadfile',
        //     'authentication' => TRUE,
        //     'resource' => 'rl1'
        // ],


    ]
];
