<?php

/**
 * @author GEEKSNEST
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */

return [
    "prefix" => "/student",
    "handler" => 'Controllers\StudentController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'POST',
            'route' => '/save',
            'function' => 'studentAddAction',
            'authentication' => false
        ],
        [
            'method' => 'GET',
            'route' => '/list/{page}/{count}/{keyword}',
            'function' => 'studentListAction',
            'authentication' => false
        ],

        [
            'method' => 'GET',
            'route' => '/info/{studentId}',
            'function' => 'studentInfoAction',
            'authentication' => false
        ],

        [
            'method' => 'GET',
            'route' => '/reg/list/{page}/{count}/{keyword}',
            'function' => 'registerListAction',
            'authentication' => false
        ],

        [
            'method' => 'GET',
            'route' => '/registrant/{regid}',
            'function' => 'regInfoAction',
            'authentication' => false
        ],


    ]
];
