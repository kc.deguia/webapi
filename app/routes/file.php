<?php

/**
 * @author GEEKSNEST
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */

return [
    "prefix" => "/file",
    "handler" => 'Controllers\FileController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'POST',
            'route' => '/upload',
            'function' => 'uploadAction',
            'authentication' => false
        ]

    ]
];
