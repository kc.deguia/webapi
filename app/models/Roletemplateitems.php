<?php

namespace Models;

class Roletemplateitems extends \Phalcon\Mvc\Model {

	public $roletemplateid;

    public function initialize() {
		$this->belongsTo("roletemplateid", "Models\Roletemplate", "id");
    }
}
