<?php

namespace Models;

class Members_language extends Basemodel
{

    public function initialize()
    {
        $this->belongsTo(
            'membersid',
            'Models\Members',
            'membersid',
            [
                "alias" => "memberLanguages",
            ]
        );
    }
    public function beforeValidationOnCreate()
    {
        $this->datecreated = date('Y-m-d H:i:s');
        $this->dateupdated = date('Y-m-d H:i:s');
    }

    public function beforeUpdate()
    {
        $this->dateupdated = date('Y-m-d H:i:s');
    }

    public static function membersLanguages($memberid)
    {
        $modeldata = Members_language::findFirst('memberid="' . $memberid . '"');
        return $modeldata;
    }

    public static function insertData($data, $memberid)
    {

        $val  = [];
        $date = date('Y-m-d H:i:s');
        $qry  = 'INSERT INTO members_language (
        memberid,
        primary_lang,
        secondary_lang,
        other_lang,
        verbal_primary,
        reading_primary,
        writing_primary,
        verbal_secondary,
        reading_secondary,
        writing_secondary,
        verbal_other,
        reading_other,
        writing_other,
        datecreated,
        dateupdated) VALUES ';

        $placeholder[] = '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        $val[]         = $memberid;
        $val[]         = $data['primary_lang'] ? $data['primary_lang']:null;
        $val[]         = $data['secondary_lang'] ? $data['secondary_lang']:null;
        $val[]         = $data['other_lang'] ? $data['other_lang']:null;
        $val[]         = $data['verbal_primary'] ? $data['verbal_primary']:null;
        $val[]         = $data['reading_primary'] ? $data['reading_primary']:null;
        $val[]         = $data['writing_primary'] ? $data['writing_primary']:null;
        $val[]         = $data['verbal_secondary'] ? $data['verbal_secondary']:null;
        $val[]         = $data['reading_secondary'] ? $data['reading_secondary']:null;
        $val[]         = $data['writing_secondary'] ? $data['writing_secondary']:null;
        $val[]         = $data['verbal_other'] ? $data['verbal_other']:null;
        $val[]         = $data['reading_other'] ? $data['reading_other']:null;
        $val[]         = $data['writing_other'] ? $data['writing_other']:null;
        $val[]         = $date;
        $val[]         = $date;

        $qry .= implode(',', $placeholder);

        // Execute the query
        try {
            $model = new Members_language();
            $model->getReadConnection()->query($qry, $val);
            return true;
        } catch (\Exception $e) {
            return $e;
        }
    }
}
