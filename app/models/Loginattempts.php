<?php

namespace Models;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Db\Column;

class Loginattempts extends Basemodel {

    public function initialize() {

    }

	public static function deleteAttempts($id, $arg=null){

		$sql = null;

		switch ($arg) {
			case 'all':
				// A raw SQL statement
				$sql = "DELETE FROM `loginattempts`
					  	WHERE userid = ? ";
				break;
			default:
				// A raw SQL statement
				$sql = "DELETE FROM `loginattempts`
					  WHERE id <= (
					    SELECT id
					    FROM (
					      SELECT userid, id
					      FROM `loginattempts`
					      ORDER BY datecreated DESC
					      LIMIT 1 OFFSET 6 -- keep this many records
					    ) foo
					  )
					";
				break;
		}

        try{
			// Base model
        	$sq = new Loginattempts();

        	// Execute the query
        	$query = $sq->getReadConnection()->query($sql, [$id]);

        	return true;
        }catch(\Exception $e){
        	die($e);
        	return $e;
        }
    }
}
