<?php

namespace Models;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Db\Column;

class Roletemplate extends Basemodel {

	public $id;

    public function initialize() {
        $this->hasManyToMany(
            "id",
            "Models\Roletemplateitems",
            "roletemplateid", "role",
            "Models\Roleitems",
            "roleCode" 
            ,[
            	"alias" => "roletemplateitems"
            ]
        );
    }

 	public static function insert($roles, $identifier){

        $placeholder = [];
        $val = [];
        $qry = 'INSERT INTO roletemplateitems (roletemplateid, role) VALUES ';
        foreach($roles as $role){
            foreach($role as $r){
                $placeholder[] = '(?, ?)';
                $val[] = $identifier;
                $val[] = $r;
            }   
        }
        $qry .= implode(',', $placeholder);

        // Execute the query
        try{
        	$model = new Roletemplate();
        	$model->getReadConnection()->query($qry, $val);
        	return true;
        }catch(\Exception $e){
        	return $e;
        }
    }


    public static function listall($page=0, $count=10,$keyword=null)
    {
        $count = $count == 'all' ? 9999 : $count;
        $offset = ($page * $count) - $count;
        $end = $offset + $count;
        $options = [
            'cols'=> [
                "id",
                "name",
                "remarks",
            ],
            'table' => 'roletemplate',
            'after_statements'=> "ORDER BY datecreated DESC LIMIT :start , :ending ",
            'params' => ["start" => ( $offset < 0 ? 0 : $offset ), "ending" => $end],
            'types' => ["start" => Column::BIND_PARAM_INT, "ending" => Column::BIND_PARAM_INT]
        ];


        if ($keyword!="null") {
            $options['conditions'] = "
                name LIKE :keyword OR 
                remarks LIKE :keyword 
            ";
            $options['params']['keyword'] = '%'.$keyword.'%';
            $options['types']['keyword'] = Column::BIND_PARAM_STR;
        }

        $sql = parent::genSQL($options);

        //Counting all records
        $options['cols'] = ['COUNT(*) as count'];
        $options['after_statements'] = 'ORDER BY datecreated DESC ';

        $sqlall = parent::genSQL($options);

        // Base model
        $templates = new Roletemplate();

        $query = $templates->getReadConnection()->query($sql, $options['params'], $options['types']);

        // Execute the query
        unset($options['params']['start'], $options['params']['ending'], $options['types']['ending'], $options['types']['start']);

        $queryall = $templates->getReadConnection()->query($sqlall, $options['params'], $options['types']);

        $pages = new Resultset(null, $templates, $queryall);
        $data = new Resultset(null, $templates, $query);

        // Get all colums pass back with its view value
        $cols = [
                "name" => true,
                "remarks" => true
            ];

        return [
                'data' => $data->toArray(), 
                'count' => $pages->toArray()[0]['count'], 
                'pages' => ceil(($pages->toArray()[0]['count'] / $count)),
                'page' => $page,
                'cols' => $cols
            ];
    }
}

