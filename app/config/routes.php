<?php


/**
 * @author GEEKSNEST fork from Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.

$routes[] = [
'method' => 'post',
'route' => '/api/update',
'handler' => 'myFunction'
];

 */


$routes[] = [
	'method' => 'post',
	'route' => '/ping',
	'handler' => ['Controllers\ExampleController', 'pingAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/test/{id}',
	'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/addmember',
    'handler' => ['Controllers\MembersController', 'addMember'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberlist',
    'handler' => ['Controllers\MembersController', 'memberList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/deletemember',
    'handler' => ['Controllers\MembersController', 'deleteMember'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/loadmemberbyid',
    'handler' => ['Controllers\MembersController', 'loadMemberbyid'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/editmember',
    'handler' => ['Controllers\MembersController', 'editMember'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberlogin',
    'handler' => ['Controllers\MembersController', 'memberLogin'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/checktoken',
    'handler' => ['Controllers\MembersController', 'checktoken'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/refreshtoken',
    'handler' => ['Controllers\MembersController', 'refreshtoken'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/checkusername',
    'handler' => ['Controllers\MembersController', 'checkUsername'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'post',
    'route' => '/members/memberuploadfile',
    'handler' => ['Controllers\MembersController', 'memberuploadfile'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/allmemberfilelist',
    'handler' => ['Controllers\MembersController', 'allmemberfilelist'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberfilelist',
    'handler' => ['Controllers\MembersController', 'memberfilelist'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberfiledelete',
    'handler' => ['Controllers\MembersController', 'memberfiledelete'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberedituploadfile',
    'handler' => ['Controllers\MembersController', 'memberedituploadfile'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/membercreatefolder',
    'handler' => ['Controllers\MembersController', 'membercreatefolder'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberdetelefolder',
    'handler' => ['Controllers\MembersController', 'memberdetelefolder'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/editfolder',
    'handler' => ['Controllers\MembersController', 'editfolder'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberdownloadfile',
    'handler' => ['Controllers\MembersController', 'memberdownloadfile'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/saveroletemplate',
    'handler' => ['Controllers\MembersController', 'saveroletemplate'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/loadroletemplatebyid',
    'handler' => ['Controllers\MembersController', 'loadroletemplatebyid'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/roletemplatelist',
    'handler' => ['Controllers\MembersController', 'roletemplatelist'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/getroletemplate',
    'handler' => ['Controllers\MembersController', 'getroletemplate'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/deleteroletemplate',
    'handler' => ['Controllers\MembersController', 'deleteroletemplate'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/editroletemplate',
    'handler' => ['Controllers\MembersController', 'editroletemplate'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/mediSource/getMemberList',
    'handler' => ['Controllers\MembersController', 'getMemberList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/loadprofile',
    'handler' => ['Controllers\MembersController', 'loadprofile'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/updateprofile',
    'handler' => ['Controllers\MembersController', 'updateprofile'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/changepassword',
    'handler' => ['Controllers\MembersController', 'changepassword'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/addsecurityquestion',
    'handler' => ['Controllers\MembersController', 'addSecurityQuestion'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/loginstatus/{memberid}',
    'handler' => ['Controllers\MembersController', 'loginstatus'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/getsecurityquestion/{memberid}',
    'handler' => ['Controllers\MembersController', 'getsecurityquestion'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/getroleitems',
    'handler' => ['Controllers\MembersController', 'getRoleitemsCb'],
    'authentication' => FALSE
];


// Patient Module Route's Start Here

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/patientMRN',
    'handler' => ['Controllers\PatientController', 'patientMRN'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/patientEmail',
    'handler' => ['Controllers\PatientController', 'patientEmail'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/patientAdd',
    'handler' => ['Controllers\PatientController', 'patientAdd'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/patientList',
    'handler' => ['Controllers\PatientController', 'patientList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/patientInfo',
    'handler' => ['Controllers\PatientController', 'patientInfo'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/patientUpdate',
    'handler' => ['Controllers\PatientController', 'patientUpdate'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/patientDelete',
    'handler' => ['Controllers\PatientController', 'deleteMember'],
    'authentication' => FALSE
];

// Physician  Module Route's Start Here
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/physicianEmail',
    'handler' => ['Controllers\PhysicianController', 'physicianEmail'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/physicianAdd',
    'handler' => ['Controllers\PhysicianController', 'physicianAdd'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/physicianList',
    'handler' => ['Controllers\PhysicianController', 'physicianList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/physicianInfo',
    'handler' => ['Controllers\PhysicianController', 'physicianInfo'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/physicianUpdate',
    'handler' => ['Controllers\PhysicianController', 'physicianUpdate'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/PhysicianDelete',
    'handler' => ['Controllers\PhysicianController', 'PhysicianDelete'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/mediSource/getPhysicianList',
    'handler' => ['Controllers\PhysicianController', 'getPhysicianList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/mediSource/getPhysicianLicense/{license}',
    'handler' => ['Controllers\PhysicianController', 'getPhysicianLicense'],
    'authentication' => FALSE
];

// Hospital  Module Route's Start Here
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/hospitalAdd',
    'handler' => ['Controllers\HospitalController', 'hospitalAdd'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/hospitalList',
    'handler' => ['Controllers\HospitalController', 'hospitalList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/hospitalInfo',
    'handler' => ['Controllers\HospitalController', 'hospitalInfo'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/hospitalUpdate',
    'handler' => ['Controllers\HospitalController', 'hospitalUpdate'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/hospitalDelete',
    'handler' => ['Controllers\HospitalController', 'hospitalDelete'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/mediSource/getHospitalList',
    'handler' => ['Controllers\HospitalController', 'getHospitalList'],
    'authentication' => FALSE
];

// External Referal  Module Route's Start Here
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/exRefAdd',
    'handler' => ['Controllers\ExternalReferalController', 'exRefAdd'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/exRefList',
    'handler' => ['Controllers\ExternalReferalController', 'exRefList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/exRefInfo',
    'handler' => ['Controllers\ExternalReferalController', 'exRefInfo'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/exRefUpdate',
    'handler' => ['Controllers\ExternalReferalController', 'exRefUpdate'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/exRefDelete',
    'handler' => ['Controllers\ExternalReferalController', 'exRefDelete'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/mediSource/getExrefList',
    'handler' => ['Controllers\ExternalReferalController', 'getExrefList'],
    'authentication' => FALSE
];

// Insurance  Module Route's Start Here
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/insuranceAdd',
    'handler' => ['Controllers\InsuranceController', 'insuranceAdd'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/insuranceList',
    'handler' => ['Controllers\InsuranceController', 'insuranceList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/insuranceInfo',
    'handler' => ['Controllers\InsuranceController', 'insuranceInfo'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/insuranceUpdate',
    'handler' => ['Controllers\InsuranceController', 'insuranceUpdate'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/insuranceDelete',
    'handler' => ['Controllers\InsuranceController', 'insuranceDelete'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/mediSource/getInsuranceList',
    'handler' => ['Controllers\InsuranceController', 'getInsuranceList'],
    'authentication' => FALSE
];

// Episode  Module Route's Start Here
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/episodeAdd',
    'handler' => ['Controllers\EpisodeController', 'episodeAdd'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/episodeList',
    'handler' => ['Controllers\EpisodeController', 'episodeList'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/episodeInfo',
    'handler' => ['Controllers\EpisodeController', 'episodeInfo'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/episodeUpdate',
    'handler' => ['Controllers\EpisodeController', 'episodeUpdate'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/mediSource/episodeDelete',
    'handler' => ['Controllers\EpisodeController', 'episodeDelete'],
    'authentication' => FALSE
];


return $routes;
