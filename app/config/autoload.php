<?php

/**
 * Auto Load Class files by namespace
 *
 * @eg
'namespace' => '/path/to/dir'
 */

$autoload = [
	'Events\Api' 				=> $dir . '/library/events/api/',
	'Utilities\Guid' 			=> $dir . '/library/utilities/guid/',
	'Utilities\Predis' 			=> $dir . '/library/utilities/predis/',
	'Events' 					=> $dir . '/library/events/',
	'Firebase\JWT' 				=> dirname($dir) . '/vendor/firebase/php-jwt/src/',
	'Application' 				=> $dir . '/library/application/',
	'Interfaces' 				=> $dir . '/library/interfaces/',
	'Controllers' 				=> $dir . '/controllers/',
	'Models' 					=> $dir . '/models/',
	'Snv\Model' 				=> $dir . '/models/snv', // Skilled Nurse Vsits Related Model
	'Sv\Model' 					=> $dir . '/models/sv', // Nurse Supervisory Visit
	'Rocorder\Model' 			=> $dir . '/models/rocorder', // Resumption of Care Order Related Model
	'Notes\Model' 				=> $dir . '/models/notes', // Notes Model
	'Oasis\Models' 				=> $dir . '/models/oasis', // Oasis Model
	'CaseSummary\Models' 		=> $dir . '/models/casesummary', // Case Summary Model & Related
	'LibraryModels' 			=> $dir . '/models/library', // Case Summary Model & Related
	'Helpers' 					=> $dir . '/helpers/',
	'RequestValidator' 			=> $dir . '/helpers/validators/',
	'RequestValidator\Custom' 	=> $dir . '/helpers/validators/custom/',
	'Micro\Responses' 			=> $dir . '/library/micro/responses/',
	'Micro\Exceptions' 			=> $dir . '/library/micro/exceptions/',
	'Security' 					=> $dir . '/library/security/',
	'Services' 					=> $dir . '/library/services/',
	'Constants' 				=> $dir . '/constants/',
	'Oasis\Post\Data' 			=> $dir . '/helpers/postdata/oasis/',
	'Notes\Post\Data' 			=> $dir . '/helpers/postdata/notes/',
	'Template\Model' 			=> $dir . '/library/template/model',
	'XML'						=> $dir . '/library/xml/',

	// CRON JOB
	'Cron\Job'					=> array(
		$dir . '/library/cronjob/',
	),

	// OASIS Data Dictionary
	'OASIS\Dic'					=> array(
		$dir . '/library/oasis/',
	),




];

return $autoload;
