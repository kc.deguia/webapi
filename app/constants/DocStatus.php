<?php

namespace Constants;

class DocStatus
{
    // General
    const READYFORQA = "Ready For QA";
    const QA = "QA Complete";
    const NA = "N/A";
    const OP = "On Progress";
    const DUE = "Overdue";
    const INFO = "Info";
    const QALOCKED = "QA Locked";
    const EXP = "Exported";
    const RO = "Reopened";

    const SEQUENCE = [
        self::OP => 0,
        self::DUE => 0,
        self::RO => 0,
        self::READYFORQA => 1,
        self::QA => 2,
    ];
    const MAX_SEQUENCE = 2;

    // Doc Title
    const MEDALPRO = "Medical / Allergy Profile";
    const MEDRECON = "Medication Reconciliations";

    const NOTES = [
        "snv"       => "SN Note",
    	"aide"     => "Aide Notes",
        "sv"       => "Supervisory Visit Note",
        "ptc"      => "Physical Therapy Clinical Notes",
        "otc"      => "Occupational Therapy Clinical Notes",
        "stc"      => "Speech Therapy Clinical Notes",
        "msw"      => "MSW Visit Notes",
        "pte"      => "Physical Theraphy Evaluation and POC",
        "ste"      => "Speech Theraphy Evaluation and POC",
        "ote"      => "Occupational Theraphy Evaluation and POC",
        "hha"      => "CHHA/CNA Assignments",


    ];

    const CARESUM = "Care Summary";
    const WOUNDCARE = "Wound Care";
    const OASIS = [
        "soc" => "Oasis Start of Care",
        "roc" => "Oasis Resumption of Care",
        "recert" => "Oasis Recertification",
        "transfer" => "Oasis Transfer",
        "death" => "Oasis Death",
        "discharge" => "Oasis Discharge"
    ];
    const ORDERS = [
        "psy" => "Physician Order",
        "adm" => "Admission Order",
        "roc" => "Resumption of Care Order",
        "rec" => "Recertification Order",
        "dis" => "Discharge Order",
        "tra" => "Transfer Order",
        "f2f" => "Physician Face to Face Encounter",
        "psyj" => "Physician Order (Justification)",
    ];
    const POC = [
        "soc" => "Plan of Care/485 (SOC)",
        "roc" => "Plan of Care/485 (ROC)",
        "rec" => "Plan of Care/485 (REC)"
    ];
    const PATIENTINFO = "Patients Medication Profile";
    const CARESUMMARY = [
        "casecon"   => "Follow-up Case Conference",
        "case60"    => "60 Day Summary",
        "case30"    => "30-Day Summary",
        "casedisins"=> "Discharge Instruction",
        "casedissum"=> "Discharge Summary",
    ];
    const CARECOORDINATION = [
        "notes"         => "Care Coordination Notes",
        "missedvisit"   => "Care Coordination Missed Visit",
    ];

    // Locations
    // const BASELINK = 'patientcare';
    const LOC = [
    	"MEDALPRO" => "patientcare/{patientid}/medication-profile",
        "MEDRECON" => "patientcare/{patientid}/{periodid}/medicaiton-reconciliations/{foreignid}",
        "WOUNDCARE" => "patientcare/{patientid}/wound/care/edit/{foreignid}",
        "OASIS" => [
            "soc" => "oasis/clinical/soc/{foreignid}/{patientid}",
            "roc" => "oasis/clinical/roc/{foreignid}/{patientid}",
            "recert" => "oasis/clinical/recert/{foreignid}/{patientid}",
            "transfer" => "oasis/clinical/transfer/{foreignid}/{patientid}",
            "death" => "oasis/clinical/death/{foreignid}/{patientid}",
            "discharge" => "oasis/clinical/discharge/{foreignid}/{patientid}"

        ],
        "PATIENTINFO" => "patientcare/{patientid}/profile",
        "POC" => [
            "soc" => "patientcare/{patientid}/{periodid}/plan-of-care/{foreignid}",
            "roc" => "patientcare/{patientid}/{periodid}/plan-of-care/{foreignid}",
            "rec" => "patientcare/{patientid}/{periodid}/plan-of-care/{foreignid}"
        ],
        "ORDERS" => [
            "psy" => "patientcare/{patientid}/{periodid}/physicianorderedit/{foreignid}/edit",
            "adm" => "patientcare/{patientid}/{periodid}/admission-order/{foreignid}",
            "roc" => "patientcare/{patientid}/{periodid}/roc-order/{foreignid}",
            "rec" => "patientcare/{patientid}/{periodid}/recertification-order/{foreignid}",
            "dis" => "patientcare/{patientid}/{periodid}/discharge-order/{foreignid}",
            "tra" => "patientcare/{patientid}/{periodid}/transfer-order/{foreignid}",
            "f2f" => "patientcare/{patientid}/{periodid}/f2f/{foreignid}",
            "psyj" => "patientcare/{patientid}/{periodid}/physician-order-justification/{foreignid}",
        ],
        "CARESUMMARY" => [
            "casecon" => "/patientcare/{patientid}/{periodid}/case/conference/none/{foreignid}",
            "case60" => "/patientcare/{patientid}/{periodid}/case/60-days-summary/{foreignid}",
            "case30" => "/patientcare/{patientid}/{periodid}/30-days/summary/{foreignid}",
            "casedisins" => "/patientcare/{patientid}/{periodid}/discharge/instruction/{foreignid}",
            "casedissum" => "/patientcare/{patientid}/{periodid}/discharge-summary/{foreignid}"
        ],
        "CARECOORDINATION" => [
            "notes" => "/patientcare/{patientid}/{periodid}/care-coordination/notes/{foreignid}",
            "missedvisit" => "/patientcare/{patientid}/{periodid}/care-coordination/missed-visit/{foreignid}",
        ],
        "NOTES" => [
            "snv"   => "/patientcare/{patientid}/{periodid}/snv/notes/none/{foreignid}",
            "aide"  => "/patientcare/{patientid}/{periodid}/chha/visit/note/none/{foreignid}",
            "sv"   => "/patientcare/{patientid}/{periodid}/snv/notes/none/{foreignid}",
            "ptc"   => "/patientcare/{patientid}/{periodid}/pt/clinical/notes/none/{foreignid}",
            "otc"   => "/patientcare/{patientid}/{periodid}/ot/clinical/notes/none/{foreignid}",
            "stc"   => "/patientcare/{patientid}/{periodid}/st/clinical/notes/none/{foreignid}",
            "msw"   => "/patientcare/{patientid}/{periodid}/msw/visit/notes/none/{foreignid}",
            "pte"   => "/patientcare/{patientid}/{periodid}/pt/evaluation/poc/none/{foreignid}",
            "ste"   => "/patientcare/{patientid}/{periodid}/st/evaluation/poc/none/{foreignid}",
            "ote"   => "/patientcare/{patientid}/{periodid}/ot/evaluation/poc/none/{foreignid}",
            "hha"   => "/patientcare/{patientid}/{periodid}/chha/assignments/note/{foreignid}",
        ],

    ];

    const TASKVISITTATUS = [
        "notcompleted",
        "completed",
        "missed",
        "onhold",
    ];
}
