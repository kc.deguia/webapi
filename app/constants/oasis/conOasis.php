<?php

/**
 * OSISIS CONSTANT VALUES
 */

$constant = array(
    'section' => array(
        "sec1"  => "clinical",
        "sec2"  => "diagnosis",
        "sec3"  => "medical",
        "sec4"  => "general",
        "sec5"  => "sensory",
        "sec6"  => "integumentary",
        "sec7"  => "enducrine",
        "sec8"  => "cardio",
        "sec9"  => "nutritional",
        "sec10" => "elimination",
        "sec11" => "neuro",
        "sec12" => "adl",
        "sec13" => "masculo",
        "sec14" => "medication",
        "sec15" => "caremngmt",
        "sec16" => "theraphy",
        "sec17" => "emergent",
    ),

    // Needed columns
    'mosOnly' => array(
        'M0'=>true,
        'M1'=>true,
        'M2'=>true,
        'm1'=>true,
        'm2'=>true),

    // Scape MO's
    'scapeFilter' => array(
        "Other"=>false,
        "other"=>false
    )
);

// OASIS Sections constant column variable

$consCol = array();

// Clinical
$consCol[$constant['section']['sec1']] =  array(
    'sec1-1' => "M0140", // Models\Oasis_m0140
    'sec1-2' => "M0150", // Models\Oasis_m0150
    'sec1-3' => "M1000", // Models\Oasis_m1000
);

// Diagnosis
$consCol[$constant['section']['sec2']] = array(
   'sec2-1' => "M1018", // Models\Oasis_m1018
   'sec2-2' => "M1028", // Models\Oasis_m1028
   'sec2-3' => "M1030", // Models\Oasis_m1030
   'sec2-4' => "M1033", // Models\Oasis_m1033
   'sec2-5' => "M1034", // Models\Oasis_m1034
   'sec2-6' => "M1036", // Models\Oasis_m1036
);

// Medical
$consCol[$constant['section']['sec3']] = array(
    'sec3-1' => "SOOpmhCardio", // Models\Oasis_pmh_cardio
    'sec3-2' => "SOOpmhGastro", // Models\Oasis_pmh_gastro
    'sec3-3' => "SOOpmhGani", // Models\Oasis_pmh_gani
    'sec3-4' => "SOOpmhEndocrine", // Models\Oasis_pmh_endocrine
    'sec3-5' => "SOOpmhMuscu", // Models\Oasis_pmh_muscu
    'sec3-6' => "SOOpmhNeuro", // Models\Oasis_pmh_neuro
    'sec3-7' => "SOOpmhInteg", // Models\Oasis_pmh_integ
    'sec3-8' => "SOOpmhCircu", // Models\Oasis_pmh_circu
    'sec3-9' => "SOOpmhSensory", // Models\Oasis_pmh_sensory
);

// General
$consCol[$constant['section']['sec4']] = array(
    'sec4-1' => "SOOpriReHoHE", // Models\Oasis_gen_hh
    'sec4-2' => "SOOhoboStat", // Models\Oasis_gen_hobo
    'sec4-3' => "SOOfuncLimi", // Models\Oasis_gen_fl
    'sec4-4' => "SOOactPerm", // Models\Oasis_gen_ap
    'sec4-5' => "SOOsafMea", // Models\Oasis_gen_sm
);

// Sensory
$consCol[$constant['section']['sec5']] = array(
   'sec5-1' => "SOOssMouth", // Models\Oasis_ss_mouth
   'sec5-2' => "SOOssNonVer", // Models\Oasis_ss_non_ver
   'sec5-3' => "SOOssNose", // Models\Oasis_ss_nose
   'sec5-4' => "SOOssPainScale", // Models\Oasis_ss_pain_scale
   'sec5-5' => "SOOssSpeech", // Models\Oasis_ss_speech
   'sec5-6' => "SOOssThroat", // Models\Oasis_ss_throat
   'sec5-7' => "SOOssTypePain",// Models\Oasis_ss_type_pain
   'sec5-8' => "SOOssWmpb", // Models\Oasis_ss_wmpb
   'sec5-9' => "SOOsite1AccLev", // Models\Oasis_site1_acc_lev
   'sec5-10' => "SOOsite1LevAfMed", // Models\Oasis_site1_lev_af_med
   'sec5-11' => "SOOsite1PresLev", // Models\Oasis_site1_pres_lev
   'sec5-12' => "SOOsite1WorstLev", // Models\Oasis_site1_worst_lev
   'sec5-13' => "SOOsite2AccLev", // Models\Oasis_site2_acc_lev
   'sec5-14' => "SOOsite2LevAfMed", // Models\Oasis_site2_lev_af_med
   'sec5-15' => "SOOsite2PresLev", // Models\Oasis_site2_pres_lev
   'sec5-16' => "SOOsite2WorstLev", // Models\Oasis_site2_worst_lev
);

// Integumentary
$consCol[$constant['section']['sec6']] = array(
   'sec6-1' => "SOOintegSkinLes", // Models\Oasis_int_skin
   'sec6-2' => "SOOintegWound", // Models\Oasis_int_wound
   'sec6-3' => "SOOcareOrder", // Models\Oasis_int_wound
   'sec6-4' => "SOOintegLesions", // Models\Oasis_int_wound
);

// Enducrine
$consCol[$constant['section']['sec7']] = array(
   'sec7-1' => "SOOendoHyper", // Models\Oasis_endu_hyper
   'sec7-2' => "SOOendoHypog", // Models\Oasis_endu_hypog
   'sec7-3' => "SOOendoDiaman", // Models\Oasis_endu_diaman
   'sec7-4' => "SOOendoBlSuMo", // Models\Oasis_endu_bloodsugar
   'sec7-5' => "SOOendoInsAdm", // Models\Oasis_endu_insuline
   'sec7-6' => "SOOendoCbsFre", // Models\Oasis_endu_cbs
   'sec7-7' => "SOOendoLeKoDi", // Models\Oasis_endu_lekodi
   'sec7-8' => "SOOendoFooInt", // Models\Oasis_endu_foot
);

// Cardio
$consCol[$constant['section']['sec8']] = array(
   'sec8-1' => "M1410", // Models\Oasis_m1410
   'sec8-2' => "M1511", // Models\Oasis_m1511
   'sec8-3' => "SOOcardBrSoDi", // Models\Oasis_cardio_diminished
   'sec8-4' => "SOOcardBrSoAb", // Models\Oasis_cardio_absent
   'sec8-5' => "SOOcardBrSoRa", // Models\Oasis_cardio_rales
   'sec8-6' => "SOOcardBrSoRh", // Models\Oasis_cardio_rhonchi
   'sec8-7' => "SOOcardBrSoWh", // Models\Oasis_cardio_wheezes
   'sec8-8' => "SOOcardBreCha", // Models\Oasis_cardio_breathchar
   'sec8-9' => "SOOcardChestPain", // Models\Oasis_cardio_chestpain
   'sec8-10' => "SOOcardRad", // Models\Oasis_cardio_rad
);

// Nutritional
$consCol[$constant['section']['sec9']] = array(
   'sec9-1' => "SOOnutNuHeSc",  // Models\Oasis_nutri_screen
   'sec9-2' => "SOOnutAbdomen", // Models\Oasis_nutri_abdomen
   'sec9-3' => "SOOnutDieReq",  // Models\Oasis_nutri_nutrition
   'sec9-4' => "SOOnutVia",     // Models\Oasis_nutri_enternal
   'sec9-5' => "SOOnutOstCar",  // Models\Oasis_nutri_ostomy
);

// Elimination
$consCol[$constant['section']['sec10']] = array(
   'sec10-1' => "SOOelimUriApp", // Models\Oasis_elim_urineapp
   'sec10-2' => "SOOelimUriCon", // Models\Oasis_elim_urinecont
   'sec10-3' => "SOOelimComplain", // Models\Oasis_elim_complain
   'sec10-4' => "SOOelimOstoCare", // Models\Oasis_elim_ostomycare
   'sec10-5' => "SOOelimTimeWeek", // Models\Oasis_elim_timeweek
   'sec10-6' => "SOOelimPeritoneal", // Models\Oasis_elim_peritoneal
   'sec10-7' => "SOOelimLowerGas", // Models\Oasis_elim_lowergas
   'sec10-8' => "SOOelimStool",// Models\Oasis_elim_stoolchar
   'sec10-9' => "SOOelimOstomycare", // Models\Oasis_elim_ostocare
);

// Neuro
$consCol[$constant['section']['sec11']] = array(
   'sec11-1' => "M1740", // Models\Oasis_m1740
   'sec11-2' => "SOOneuOrientation", // Models\Oasis_neuro_disoriented
   'sec11-3' => "SOOneuLevelCon", // Models\Oasis_neuro_levelcon
   'sec11-4' => "SOOneuOtherSign", // Models\Oasis_neuro_sign
   'sec11-5' => "SOOneuLearning", // Models\Oasis_neuro_learning
   'sec11-6' => "SOOneuObservation", // Models\Oasis_neuro_observation
   'sec11-7' => "SOOneuSignAbuse", // Models\Oasis_neuro_signabuse
);

// Masculokeletal
$consCol[$constant['section']['sec13']] = array(
   'sec13-1' => "SOOmasculoSkeletal", // Models\Oasis_mascu_skeletal
);

// Medication
$consCol[$constant['section']['sec14']] = array(
   'sec14-1' => "SOOmedIntraAcc", // Models\Oasis_med_intravenous
);

// Theraphy
$consCol[$constant['section']['sec16']] = array(
   'sec16-1'  => "SOOtheCoordination", // Models\Oasis_thera_coordination
   'sec16-2'  => "SOOtheCommunicated", // Models\Oasis_thera_communicated
   'sec16-3'  => "SOOtheMedication", // Models\Oasis_thera_medication
   'sec16-4'  => "SOOthePatientCon", // Models\Oasis_thera_patientcon
   'sec16-5'  => "SOOtheObtainPhy", // Models\Oasis_thera_obtainphy
   'sec16-6'  => "SOOthePhyTheraphy", // Models\Oasis_thera_phytheraphy
   'sec16-7'  => "SOOtheOccuTheraphy", // Models\Oasis_thera_occutheraphy
   'sec16-8'  => "SOOtheSpeechTheraphy", // Models\Oasis_thera_speech
   'sec16-9'  => "SOOtheMsw", // Models\Oasis_thera_msw
   'sec16-10' => "SOOtheSumSkill", // Models\Oasis_thera_sumskill
   'sec16-11' => "SOOtheDischargePlan", // Models\Oasis_thera_dischargeplan
   'sec16-12' => "SOOtheInstruction", // Models\Oasis_thera_instruction
);

// Emergent
$consCol[$constant['section']['sec17']] = array(
   'sec17-1' => "M2310", // Models\Oasis_m2310
   'sec17-2' => "M2430", // Models\Oasis_m2430
);

$consCol = array('consCol' => $consCol);
// Merge Section & Model
//
$secModel = array_merge($constant, $consCol);
// End  OASIS Sections constant column variable



return $secModel;
